/*
SQLyog Professional v12.08 (64 bit)
MySQL - 8.0.21 : Database - hps
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`hps` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE hps;

/*Data for the table address_ */

insert  into address_
	(address_id,credit_card_id,customer_id,addr_country,addr_state,addr_city,addr_area,addr_detail,postal_code) 
values 
	(8,1,23,'USA','Hawaii','Moanalua','245 Randall Drive 96819','245 Randall Drive 96819',NULL),
	(9,20,23,'USA','Utah','Park City',NULL,'1024 Burnside Avenue',NULL),
	(10,1,27,'USA','Michigan','Southfield',NULL,'2344 Lakeland Terrace 48075',NULL),
	(11,1,27,'USA','New Mexico','Belen',NULL,'77 Chapmans Lane ',NULL),
	(12,28,28,'USA','Hawaii','Moanalua',NULL,'245 Randall Drive ','96819'),
	(13,27,28,'UK',NULL,'HACKNESS',NULL,'99 Moulton Road','KW16 5QA'),
	(14,29,28,'UK',NULL,'FIMBER',NULL,'57 Rhosddu Rd  ','YO25 0RQ');

/*Data for the table airline_ */

insert  into airline_
	(airline_id,airline_name,airline_code,country_) 
values 
	(1,'Spring Airlines','9C','China'),
	(2,'Juneyao Airlines','HO','China'),
	(3,'Loong Air','GJ','China'),
	(4,'China Xinhua Airlines','X2','China'),
	(5,'Zhongyuan Airlines','Z2 ','China'),
	(6,'American Airlines','AA','USA'),
	(7,'China Southern Airlines','CZ','China'),
	(8,'China Eastern Airlines','MU','China'),
	(9,'Xiamen Airlines Company','MF','China'),
	(10,'Air China International Corp.','CA','China'),
	(11,'Shanghai Airlines','FM','China'),
	(12,'Korean Air Lines Co.Ltd','KE','South Korea'),
	(13,'Adiana Airlines ','OZ','South Korea'),
	(14,'Japan Airlines Company Lts.','JL','Japan'),
	(15,'All Nippon Airways ','NH','Japan'),
	(16,'Singapore Airlines ','SQ','Singapore'),
	(17,'Thai Airways International','TG','Thailand'),
	(18,'United Airlines ','UA','USA'),
	(19,'Brited Airways ','BA','UK'),
	(20,'Klm Royal Dutch Airlines','KL',' Netherlands'),
	(21,'Lufthansa Cargo AG','LH','Germany'),
	(22,'Air France','AF','France'),
	(23,' Swiss International Air Lines','LX','Switzerland'),
	(24,'Austrian Airlines','OS','Austria'),
	(25,'Aeroflot Russian International','SU','Russia'),
	(26,'Qantas Airways','QF','Australia'),
	(27,'Finnair Airlines','AY','Finland'),
	(28,' Emirates Airlines','EK','Dubai'),
	(29,'Scandinavian Airlines','SK','Stockholm'),
	(30,'Royal Brunei Airlines','BI','Brunei'),
	(31,'Garuda Indonesia Airlines','GA','Indonesia'),
	(32,'SilkAir(Singapore) Pty.Ltd','MI','Singapore'),
	(33,'Malaysian Airlines System Berhad','MH','Malaysia'),
	(34,'Ethiopian Airlines Enterprise','ET','Ethiopia'),
	(35,'Lot-Polish Airlines','LO','Poland'),
	(36,'Tarom Romanian Air Transport','RO','Romania'),
	(37,'EVA Airways Corporation','BR','China'),
	(38,'Air Ukraine','6U','Ukraine'),
	(39,'Pakistan International Airlines','PK',' Pakistan'),
	(40,'Philippine Airlines','PR','Philippines'),
	(41,'Royal Nepal Airlines','RA','Nepal'),
	(42,'Iranian Airways','IR','Iran'),
	(43,'Air Koryo','JS','Korea'),
	(44,'EL AL Israel Airlines Ltd.','LY','Israel'),
	(45,'Air Macau Company Ltd.','NX','China'),
	(46,'Myanmar Airways Int\'l','UB','Myanmar'),
	(47,'Vietnam Airlines','VN','Vietnam'),
	(48,'China Airlines','CI','China'),
	(49,'Cathay Pacific Airways','CX','China'),
	(50,'China Southwest Airlines','SZ','China'),
	(51,'American Airlines','AA','USA'),
	(52,'Turkish Airlines','TK','Turkey'),
	(53,'Saudi Arabian Airlines','SV','Saudi Arabia'),
	(54,'South African airways','SA','South Africa'),
	(55,'LUXAIR Airlines ','LG','Germany'),
	(56,'Air New Zealand Ltd.','NZ','New Zealand'),
	(57,'Virgin Atlantic Airways Ltd.','VS','UK'),
	(58,'Srilankan Airlines Ltd.','UL','Sri Lanka'),
	(59,'Eurocypria Airlines','UI','Cyprus'),
	(60,'Kuwait Airways','KU','Kuwait'),
	(61,'Royal Jordanian Airline','RJ','Jordan'),
	(62,'Delta Airlines Inc.','DL','USA'),
	(63,'Air India Ltd.','AI','India'),
	(64,'Air Jamaica','JM','Jamaica'),
	(65,'Alaska Airlines Inc.','AS','USA'),
	(66,'CSA Czerk Airlines','OK','Czech'),
	(67,'Iceland Air','FI','Iceland'),
	(68,'Lauda Air','NG','Austria'),
	(69,'Malev Hungarian Airlines','MA','Hungary'),
	(70,'Olympic Airways','OA','Greece'),
	(71,' Aerolineas Argentinas','AR','Argentina'),
	(72,'Air canada','AC','Canada'),
	(73,'Air Hongkong','HX','China'),
	(74,'Air Madagascar','MD','Madagascar'),
	(75,'Air Mauritius','MK','Mauritius'),
	(76,'Alitalia Airlines','AZ','Italy'),
	(77,' Southwest Airlines','WN','USA'),
	(78,' Bangkok Airways','PG','Thailand'),
	(79,' Egypt Air','MS','Egypt'),
	(80,' Japan Asia Airways','EG','Japan'),
	(81,'Qatar Airways','QR','Qatar'),
	(83,'Taiyuan Airlines','TY','China');

/*Data for the table airport_ */

insert  into airport_
	(airport_id,IATA_,airport_name,country_,state_,city_,addr_detail_1,addr_detail_2,airport_telephone,is_domestic) 
values 
	(8,'BOS','General Edward Lawrence Logan International Airport','USA','Massachusetts','Boston',NULL,NULL,'0870 000 8593',2),
	(11,'LGW','London Gatwick Airport ','UNITED KINGDOM',NULL,'London',NULL,NULL,'0870 000 2468',2),
	(12,'GSO','Piedmont Triad International Airport','USA','North Carolina','Greensboro',NULL,NULL,'5890 000 2423',2),
	(14,'CAN','Guangzhou Baiyun International Airport','CHINA','Guangdong Province','guangzhou',NULL,NULL,'416-2457-1286',1),
	(15,'PKX','Beijing Daxing International Airport','CHINA','beijing','beijing',NULL,NULL,'256-8614-3652',1),
	(18,'CLT','Charlotte Douglas International Airport','USA','North Carolina','Charlotte',NULL,NULL,'267 9149 4361',2),
	(19,'PHL','Philadelphia International Airport ','USA','Commonwealth of Pennsylvania','Philadelphia',NULL,NULL,'+1 215 937 6937',1),
	(20,'PHX','Phoenix Sky Harbor International Airport','USA','State of Arizona','Phoenix',NULL,NULL,'(602) 273-3300',2),
	(21,'LAS','McCarran International Airport','USA','Nevada','Las Vegas',NULL,NULL,'+1 702 261 5211',2),
	(22,'PIT','Pittsburgh International Airport','USA','Commonwealth of Pennsylvania','Pittsburgh',NULL,NULL,'412 472 3525',2),
	(23,'LGA','La Guardia Airport','USA','State of New York','New York',NULL,NULL,'+1 (718) 533 3400',2),
	(24,'DCA',' Ronald Reagan Washington National Airport','USA',NULL,'Washington D.C',NULL,NULL,'012-5812-3641',2),
	(25,'BDL','Bradley International Airport','USA','Connecticut','Hartford',NULL,NULL,'+1 860 292 2000',2),
	(26,'INN',' Innsbruck Kranebitten Airport','Austria','Tyrol','Innsbruck Main',NULL,NULL,'213 5135 3525',2),
	(27,'AMS','Schiphol Airport','The Netherlands','Noord-Holland','Amsterdam',NULL,NULL,'+31 20 794 0800',2),
	(28,'JDH','Jodhpur Airport','INDIA','Rajasthan','Jodhpur',NULL,NULL,'91-291-2512934',2),
	(29,'DEL','Indira Gandhi ','INDIA','National Capital Territory of Delhi','New Delhi',NULL,NULL,'514-919-3504',2),
	(30,'SFO','San Francisco International Airport','USA','State of California','San Francisco',NULL,NULL,'078 1026 2839',2),
	(31,'TSN','Tianjin Binhai International Airport','CHINA',NULL,'Tianjin',NULL,NULL,'705-606-0814',1),
	(32,'SZX','Shenzhen Bao\'an International Airport','CHINA','Guangdong Province','Shenzhen',NULL,NULL,'418-929-7871',1),
	(33,'WUX','Sunan Shuofang International Airport','CHINA','Jiangsu','Wuxi',NULL,NULL,'505-864-1524',1),
	(34,'BJR','Bahir Dar Airport','ETHIOPIA',NULL,'Bahir Dar',NULL,NULL,'805-590-8516',2),
	(35,'KMG','Kunming Changshui International Airport','CHINA','Yunnan','Kunming',NULL,NULL,'078 1026 2839',1),
	(36,'OGG','Kahului Airport','USA','Hawaii State','Kahului ',NULL,NULL,'+1 (0)808 8723893',2),
	(37,'DXB','Dubai International Airport','United Arab Emirates','Imārat Dubayy','Dubai',NULL,NULL,'514-919-3504',2),
	(38,'KWE','Guiyang Longdongbao International Airport','CHINA','Guizhou','Guiyang',NULL,NULL,'514-919-3504',1),
	(39,'MFM','Macau International Airport','CHINA','Macao','Macao',NULL,NULL,'418-929-7871',1),
	(40,'JJN','Quanzhou Jinjiang International Airport','CHINA','Fujian','Quanzhou',NULL,NULL,'517-775-9746',1),
	(41,'TYN','Taiyuan Airport','CHINA ','shanxi','Taiyuan',NULL,NULL,'256-8546-5385',1);

/*Data for the table booking_ */

insert  into booking_
	(booking_id,booking_number,customer_id,flight_id,seat,state_,telephone,certificate_id,credit_card_id,departure_airport,departure_time,destination_airport,destination_time,mileage_sum,price,ticket_sum,price_sum,create_date,update_date) 
values 
	(1,'BOK-1619076047208',20,5,1,2,NULL,'[2,1]',1,2,'2021-04-24 14:53:28',3,'2021-04-30 10:58:44',NULL,'90.00',2,'180.00','2021-04-23 15:58:07',NULL),
	(4,'BOK-1619169039109',20,12,2,2,NULL,'[2,1]',1,2,'2021-04-24 00:00:00',4,'2021-04-24 12:00:00',NULL,'600.00',2,'1200.00','2021-04-23 17:10:38','2021-04-23 19:29:59'),
	(5,'BOK-1619169043455',20,12,1,2,NULL,'[2,1]',1,2,'2021-04-24 00:00:00',4,'2021-04-24 12:00:00',NULL,'1280.00',2,'2560.00','2021-04-23 17:10:42','2021-04-23 18:19:05'),
	(6,'BOK-1619169088468',20,12,1,2,NULL,'[2]',1,2,'2021-04-24 00:00:00',4,'2021-04-24 12:00:00',NULL,'1280.00',1,'1280.00','2021-04-23 17:11:27','2021-04-23 18:16:24'),
	(7,'BOK-1619169101412',20,12,2,2,NULL,'[2]',1,2,'2021-04-24 00:00:00',4,'2021-04-24 12:00:00',NULL,'600.00',1,'600.00','2021-04-23 17:11:40','2021-04-23 18:16:12'),
	(8,'BOK-1619176527275',12,13,1,2,NULL,'[2]',111,2,'2021-04-24 00:00:00',4,'2021-04-24 12:00:00',450,'1280.00',1,'1280.00','2021-04-23 19:15:26','2021-04-23 19:40:59'),
	(9,'BOK-1619177060814',12,12,1,2,NULL,'[12]',11,2,'2021-04-24 00:00:00',4,'2021-04-24 12:00:00',450,'1280.00',1,'1280.00','2021-04-23 19:24:20','2021-04-26 09:30:56'),
	(10,'BOK-1619177592382',20,12,2,2,NULL,'[2,4]',1,2,'2021-04-24 00:00:00',4,'2021-04-24 12:00:00',450,'600.00',2,'1200.00','2021-04-23 19:33:11','2021-04-23 19:33:59'),
	(11,'BOK-1619418491051',20,15,2,2,NULL,'[5]',1,2,'2021-04-27 12:00:00',3,'2021-04-28 00:00:00',600,'800.00',1,'800.00','2021-04-26 14:28:10','2021-04-28 17:37:00'),
	(12,'BOK-1619597559970',20,22,2,2,NULL,'[2,4]',NULL,5,'2021-04-28 23:00:00',6,'2021-04-29 00:00:00',0,'450.00',2,'900.00','2021-04-28 16:12:40','2021-04-28 17:40:00'),
	(13,'BOK-1619685160048',20,23,2,2,NULL,'[2]',NULL,14,'2021-05-02 08:20:00',15,'2021-05-02 11:11:04',0,'469.00',1,'469.00','2021-04-29 16:32:39','2021-05-05 11:18:00'),
	(14,'BOK-1619686206647',23,25,2,1,NULL,'[7]',NULL,11,'2021-05-12 21:29:00',8,'2021-05-13 05:28:00',0,'1234.00',1,'1234.00','2021-04-29 16:50:06',NULL),
	(15,'BOK-1619686217038',23,24,2,1,NULL,'[7]',NULL,15,'2021-05-04 16:38:00',14,'2021-05-04 18:44:00',0,'539.00',1,'539.00','2021-04-29 16:50:16',NULL),
	(16,'BOK-1619687714339',20,23,2,2,NULL,'[5]',NULL,14,'2021-05-02 08:20:00',15,'2021-05-02 11:11:04',0,'469.00',1,'469.00','2021-04-29 17:15:13','2021-05-05 11:18:00'),
	(17,'BOK-1619689567297',28,25,1,3,NULL,'[10,11,12]',NULL,11,'2021-05-12 21:29:00',8,'2021-05-13 05:28:00',0,'865.00',3,'2595.00','2021-04-29 17:46:06','2021-04-29 17:47:07'),
	(18,'BOK-1619689932915',28,24,2,3,NULL,'[12]',NULL,15,'2021-05-04 16:38:00',14,'2021-05-04 18:44:00',0,'539.00',1,'539.00','2021-04-29 17:52:12','2021-05-07 17:22:37'),
	(19,'BOK-1619691316392',28,23,2,2,NULL,'[10,11,12]',NULL,14,'2021-05-02 08:20:00',15,'2021-05-02 11:11:04',0,'469.00',3,'1407.00','2021-04-29 18:15:15','2021-05-05 11:17:00'),
	(20,'BOK-1619692307931',28,31,2,2,NULL,'[11]',NULL,32,'2021-04-30 00:00:00',31,'2021-04-30 05:00:00',0,'790.00',1,'790.00','2021-04-29 18:31:47','2021-04-30 14:20:17'),
	(21,'BOK-1619692623640',28,24,2,1,NULL,'[11]',NULL,15,'2021-05-04 16:38:00',14,'2022-06-09 18:44:00',0,'539.00',1,'539.00','2021-04-29 18:37:03',NULL),
	(22,'BOK-1620203855374',28,49,1,3,NULL,'[12,11,10]',NULL,33,'2021-05-06 04:34:38',35,'2021-05-06 05:00:45',0,'0.00',3,'0.00','2021-05-05 16:37:35','2021-05-05 16:38:36'),
	(23,'BOK-1620203930019',28,49,2,2,NULL,'[12,11,10]',NULL,33,'2021-05-06 04:34:38',35,'2021-05-06 05:00:45',0,'499.00',3,'1497.00','2021-05-05 16:38:50','2021-05-06 15:57:00'),
	(24,'BOK-1620204114008',28,25,2,3,NULL,'[10,11,12]',NULL,11,'2021-05-12 21:29:00',8,'2021-05-13 05:28:00',0,'1234.00',3,'3702.00','2021-05-05 16:41:54','2021-05-07 17:23:29'),
	(25,'BOK-1620204162037',28,50,1,2,NULL,'[10,11,12]',NULL,18,'2021-05-06 07:40:48',8,'2021-05-07 03:40:54',0,'5978.00',3,'17934.00','2021-05-05 16:42:42','2021-05-07 09:25:01'),
	(26,'BOK-1620208100391',28,61,1,1,NULL,'[12,11,10]',NULL,27,'2021-08-05 06:14:22',28,'2021-08-05 10:14:28',0,'576.00',3,'1728.00','2021-05-05 17:48:20',NULL),
	(27,'BOK-1620211241532',20,32,1,1,NULL,'[4,5]',NULL,35,'2022-04-11 15:00:00',32,'2022-04-11 18:00:00',1060,'954.00',2,'1908.00','2021-05-05 18:40:41',NULL),
	(28,'BOK-1620211361799',20,26,1,1,NULL,'[2,5,4]',NULL,12,'2022-04-20 14:15:00',8,'2022-04-21 00:11:00',4194,'1650.00',3,'4950.00','2021-05-05 18:42:41',NULL),
	(29,'BOK-1620284798668',28,33,2,1,NULL,'[13,12]',NULL,25,'2022-06-15 12:00:00',22,'2022-06-16 10:00:00',15304,'2308.00',2,'4616.00','2021-05-06 15:06:38',NULL),
	(30,'BOK-1620288286520',28,75,2,2,NULL,'[13,12]',NULL,12,'2021-05-06 21:00:00',18,'2021-05-06 22:00:00',312,'1200.00',2,'2400.00','2021-05-06 16:04:46','2021-05-07 09:25:00'),
	(31,'BOK-1620288463216',28,76,1,2,NULL,'[12,10]',NULL,14,'2021-05-06 21:15:00',19,'2021-05-07 00:29:00',11724,'865.00',2,'1730.00','2021-05-06 16:07:42','2021-05-07 09:25:00'),
	(32,'BOK-1620356195670',28,36,2,1,NULL,'[10,11]',NULL,38,'2022-10-20 02:53:00',15,'2022-10-20 05:14:00',794,'596.00',2,'1192.00','2021-05-07 10:56:35',NULL);

/*Data for the table certificate */

insert  into certificate
	(certificate_id,customer_id,passenger_name,passengers_telephone,certificate_num) 
values 
	(7,23,'William C Emanuel','808-840-3230','220802198307261517'),
	(8,26,'xiaowang','13595426513','425231199504302560'),
	(9,27,'xiaoMeizu','722 560 499','340103197305280037'),
	(10,28,'user','13586224936','445262198709253614'),
	(11,28,'liming','12358462541','125423188607153251'),
	(12,28,'xiaohong','13356241526','125345198002153512'),
	(13,28,'molihua','129-5723-5241','552130195807295054');

/*Data for the table credit_card */

insert  into credit_card
	(credit_card_id,customer_id,name_,card_number,bank_,expiration,cvccode) 
values 
	(19,23,'MasterCard','5571172688086938','William C Emanuel','2025-04-16 00:00:00','5263'),
	(20,23,'MasterCard','4916625264846030','William C Emanuel','2026-04-14 00:00:00','1253'),
	(22,27,'Visa','4556649817035839','xiaoMeizu','2024-04-17 00:00:00','2563'),
	(24,27,'Visa','4916362295399777','xiaoMeizu','2025-04-18 00:00:00','2563'),
	(25,20,'nonghang','123321','niuxiaoer','2021-04-30 00:00:00','3211'),
	(27,28,'nonghang','5476384253613717','user','2024-04-18 00:00:00','1235'),
	(28,28,'jianshe','3550900550803810','user','2023-04-13 00:00:00','5236'),
	(29,28,'zhaoshang','5363261187519234','user','2025-07-17 00:00:00','2541');

/*Data for the table customer */

insert  into customer
	(customer_id,customer_name,email,sex,country,state,portrait,city,password,pay_password,telephone,Identity,airport_id,create_date,create_by,update_datae,update_by,mileage_num) 
values 
	(3,NULL,'111@163.com',1,NULL,NULL,NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,0),
	(4,NULL,'1112@163.com',1,NULL,NULL,NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,0),
	(5,NULL,'123@163.com',1,NULL,NULL,NULL,NULL,'202cb962ac59075b964b07152d234b70',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,0),
	(6,NULL,'112@163.com',1,NULL,NULL,NULL,NULL,'202cb962ac59075b964b07152d234b70','202cb962ac59075b964b07152d234b70',NULL,1,NULL,NULL,NULL,NULL,NULL,0),
	(7,NULL,'admin@163.com',1,'China','Guangdong','admin','Guanzhou','96e79218965eb72c92a549dd5a330112',NULL,NULL,2,15,NULL,NULL,NULL,NULL,0),
	(8,'aa','a@b.com',1,NULL,NULL,NULL,NULL,'','47bce5c74f589f4867dbd57e9ca9f808',NULL,1,NULL,NULL,NULL,NULL,NULL,0),
	(9,'aaa','a@c.com',1,NULL,NULL,NULL,NULL,'','47bce5c74f589f4867dbd57e9ca9f808',NULL,1,NULL,NULL,NULL,NULL,NULL,0),
	(10,'aa','a@v.com',1,NULL,NULL,NULL,NULL,'','47bce5c74f589f4867dbd57e9ca9f808',NULL,1,NULL,NULL,NULL,NULL,NULL,0),
	(14,'nick','113@b.com',1,NULL,NULL,NULL,NULL,'b59c67bf196a4758191e42f76670ceba',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,0),
	(16,'nick','11@a.com',1,NULL,NULL,NULL,NULL,'b59c67bf196a4758191e42f76670ceba',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,0),
	(17,'nick','113@q.com',1,NULL,NULL,NULL,NULL,'b59c67bf196a4758191e42f76670ceba',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,0),
	(18,'11','12@a.com',1,NULL,NULL,NULL,NULL,'b59c67bf196a4758191e42f76670ceba',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,0),
	(19,'13123','1@c.com',1,NULL,NULL,NULL,NULL,'b59c67bf196a4758191e42f76670ceba',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,0),
	(21,'nick','113@163.com',1,NULL,NULL,NULL,NULL,'81dc9bdb52d04dc20036dbd8313ed055',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,0),
	(23,'William C Emanuel','08qiiigc4fih@temporary-mail.net',NULL,'USA','Hawaii','William C Emanuel','Moanalua','e10adc3949ba59abbe56e057f20f883e',NULL,NULL,1,8,NULL,NULL,NULL,NULL,0),
	(24,'Kassie B Knowles','gi5phoy0k8n@temporary-mail.net',NULL,NULL,NULL,NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,0),
	(25,'Lee J Brown','uk8f7fc4ryt@temporary-mail.net',NULL,NULL,NULL,NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,0),
	(26,'xiaowang','123456@136.com',NULL,NULL,NULL,NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,0),
	(27,'xiaoMeizu','123456@163.com',1,'USA','Michigan','xiaoMeizu','Southfield','e10adc3949ba59abbe56e057f20f883e',NULL,NULL,1,11,NULL,NULL,NULL,NULL,0),
	(28,'user','user@163.com',2,'UNITED KINGDOM',NULL,'user','London','e10adc3949ba59abbe56e057f20f883e',NULL,NULL,1,11,NULL,NULL,NULL,NULL,12036),
	(29,'fd','adf@163.com',NULL,NULL,NULL,NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,0);

/*Data for the table flight_ */

insert  into flight_
	(flight_id,airline_id,flight_number,flight_date,start_address,destination_,start_date,arrive_date,first_class_num,economy_class_num,first_class_price,economy_class_price,mileage_sum,departure_airport,destination_airport) 
values 
	(23,8,'UAE363','2021-05-02 08:20:00',NULL,NULL,'2021-05-02 08:20:00','2021-05-02 11:11:04',5,265,'539.00','469.00',869,14,15),
	(24,7,'UAE242','2022-04-07 18:40:10',NULL,NULL,'2022-04-07 18:40:10','2022-04-07 21:40:10',3,234,'632.00','539.00',896,15,14),
	(25,9,'CSN3115','2021-05-12 21:29:00',NULL,NULL,'2021-05-12 21:29:00','2021-05-13 05:28:00',19,263,'865.00','1234.00',1305,11,8),
	(26,7,'UAE272','2022-04-20 14:15:00',NULL,NULL,'2022-04-20 14:15:00','2022-04-21 00:11:00',8,260,'1650.00','1322.00',1563,12,8),
	(27,9,'JAL31','2021-04-29 18:03:00',NULL,NULL,'2021-04-29 18:03:00','2022-04-05 23:10:00',5,156,'356.00','253.00',563,26,23),
	(28,12,'CXA8474','2021-04-29 18:10:00',NULL,NULL,'2021-04-29 18:10:00','2021-04-29 19:20:00',8,125,'366.00','530.00',533,29,20),
	(29,10,'THY2718 ','2022-05-18 10:00:00',NULL,NULL,'2022-05-18 10:00:00','2022-05-18 15:11:00',12,246,'855.00','688.00',1560,24,21),
	(30,9,'KAL529','2021-04-29 17:00:00',NULL,NULL,'2021-04-29 17:00:00','2021-04-30 00:11:00',12,213,'896.00','685.00',1256,21,27),
	(31,8,'CSN3129','2021-05-13 00:00:00',NULL,NULL,'2021-05-13 00:00:00','2021-05-14 05:00:00',31,254,'960.00','790.00',860,32,31),
	(32,8,'UAE343','2022-04-11 15:00:00',NULL,NULL,'2022-04-11 15:00:00','2022-04-11 18:00:00',8,254,'954.00','685.00',530,35,32),
	(33,17,'THY2718 ','2022-06-15 12:00:00',NULL,NULL,'2022-06-15 12:00:00','2022-06-16 10:00:00',21,268,'2694.00','2308.00',11698,25,22),
	(34,8,'UAE216','2022-09-15 08:00:00',NULL,NULL,'2022-09-15 08:00:00','2022-09-15 20:00:00',36,246,'2856.00','2369.00',11362,35,23),
	(35,8,'UEZ256','2021-04-29 19:04:00',NULL,NULL,'2021-04-29 19:04:00','2021-04-29 21:02:00',8,156,'669.00','530.00',536,31,14),
	(36,50,'CSN3119','2022-10-20 02:53:00',NULL,NULL,'2022-10-20 02:53:00','2022-10-20 05:14:00',10,264,'899.00','596.00',756,38,15),
	(37,48,'CSN301','2022-03-13 16:42:00',NULL,NULL,'2022-03-13 16:42:00','2022-03-13 19:07:00',12,126,'747.00','569.00',852,35,39),
	(38,10,'CSN3194','2022-09-21 18:18:00',NULL,NULL,'2022-09-21 18:18:00','2022-09-21 22:22:00',20,126,'589.00','385.00',593,40,14),
	(39,11,'UAE216','2023-01-27 06:24:00',NULL,NULL,'2023-01-27 06:24:00','2023-01-27 09:12:00',10,256,'695.00','493.00',685,15,33),
	(40,48,'UAE203 ','2022-02-18 08:54:00',NULL,NULL,'2022-02-18 08:54:00','2022-02-18 12:11:00',12,126,'750.00','496.00',453,14,39),
	(41,77,'ASA221','2022-11-21 02:09:38',NULL,NULL,'2022-11-21 02:09:38','2022-11-21 06:15:00',8,138,'652.00','496.00',462,38,14),
	(42,12,'ANA212','2022-12-11 17:10:00',NULL,NULL,'2022-12-11 17:10:00','2022-12-12 06:04:00',32,268,'2895.00','2596.00',11586,26,20),
	(43,46,'ANA115','2022-09-22 03:07:00',NULL,NULL,'2022-09-22 03:07:00','2022-09-22 11:09:00',38,266,'986.00','795.00',11593,36,34),
	(44,9,'ASA378','2022-08-25 23:24:00',NULL,NULL,'2022-08-25 23:24:00','2022-08-26 03:03:00',8,124,'685.00','495.00',596,15,39),
	(45,8,'ASA523','2022-02-14 04:09:00',NULL,NULL,'2022-02-14 04:09:00','2022-02-14 07:45:00',12,128,'798.00','508.00',493,38,32),
	(46,54,'CSN3194','2022-03-14 02:40:00',NULL,NULL,'2022-03-14 02:40:00','2022-03-14 07:10:00',8,126,'596.00','389.00',536,33,14),
	(47,48,'CSN3924','2022-09-30 21:36:00',NULL,NULL,'2022-09-30 21:36:00','2022-10-01 00:40:00',12,248,'726.00','608.00',596,33,15),
	(48,81,'ANA115','2022-08-15 17:32:00',NULL,NULL,'2022-08-15 17:32:00','2022-08-16 07:50:00',38,248,'1325.00','896.00',9824,36,34),
	(49,77,'UAE325','2021-05-06 04:34:38',NULL,NULL,'2021-05-06 04:34:38','2021-05-06 05:00:45',53,129,'652.00','499.00',395,33,35),
	(50,12,'UFS156','2021-05-06 07:40:48',NULL,NULL,'2021-05-06 07:40:48','2021-05-07 03:40:54',38,355,'5978.00','4860.00',13862,18,8),
	(51,16,'TDA1594','2021-05-05 12:08:00',NULL,NULL,'2021-05-05 12:08:00','2021-05-05 17:00:00',28,126,'875.00','697.00',5930,23,22),
	(52,14,'TIU265','2022-08-18 18:35:00',NULL,NULL,'2022-08-18 18:35:00','2022-08-19 08:35:00',56,358,'1268.00','899.00',9686,12,18),
	(53,15,'DRG2569','2022-05-20 03:00:00',NULL,NULL,'2022-05-20 03:00:00','2022-05-20 21:00:00',28,125,'1698.00','985.00',8756,8,11),
	(54,81,'FWA892','2022-07-22 08:56:00',NULL,NULL,'2022-07-22 08:56:00','2022-07-22 21:08:00',26,256,'1976.00','1587.00',9756,12,14),
	(55,79,'VFG5666','2022-08-24 13:50:00',NULL,NULL,'2022-08-24 13:50:00','2022-08-25 10:08:00',28,287,'1777.00','1585.00',7856,15,18),
	(56,76,'FYH256','2022-10-04 19:50:00',NULL,NULL,'2022-10-04 19:50:00','2022-10-05 10:40:00',24,268,'1589.00','1129.00',7858,19,20),
	(57,72,'TGF5896','2022-11-22 03:26:00',NULL,NULL,'2022-11-22 03:26:00','2022-11-22 10:02:00',38,145,'1517.00','1190.00',8757,21,22),
	(58,74,'GFD746','2022-07-26 19:02:00',NULL,NULL,'2022-07-26 19:02:00','2022-07-27 07:43:00',23,348,'996.00','756.00',3895,23,25),
	(59,76,'YUG1253','2021-05-05 04:11:05',NULL,NULL,'2021-05-05 04:11:05','2021-05-05 14:11:08',28,269,'1798.00','1369.00',7893,23,24),
	(60,70,'FGS251','2022-06-27 04:12:26',NULL,NULL,'2022-06-27 04:12:26','2022-06-27 14:12:29',28,258,'1782.00','1385.00',8742,25,26),
	(61,66,'GFD652','2021-08-05 06:14:22',NULL,NULL,'2021-08-05 06:14:22','2021-08-05 10:14:28',8,149,'576.00','354.00',3862,27,28),
	(62,57,'RFU2663','2022-09-21 14:12:00',NULL,NULL,'2022-09-21 14:12:00','2022-09-22 04:06:00',22,256,'885.00','681.00',5395,29,30),
	(63,56,'GUB468','2022-05-08 02:09:00',NULL,NULL,'2022-05-08 02:09:00','2022-05-08 11:05:00',24,198,'1879.00','1682.00',7468,31,34),
	(64,59,'YTF4896','2022-05-13 19:09:00',NULL,NULL,'2022-05-13 19:09:00','2022-05-14 07:09:00',22,256,'1235.00','856.00',6821,38,40),
	(65,17,'GTR268','2022-05-27 00:05:00',NULL,NULL,'2022-05-27 00:05:00','2022-05-27 11:03:00',28,258,'1862.00','1566.00',9875,12,22),
	(66,18,'SDR256','2022-05-25 16:06:00',NULL,NULL,'2022-05-25 16:06:00','2022-05-26 00:30:00',26,128,'895.00','658.00',7620,14,11),
	(67,14,'GTR259','2022-06-18 05:28:00',NULL,NULL,'2022-06-18 05:28:00','2022-06-18 11:02:00',28,159,'1982.00','1586.00',9698,14,24),
	(68,19,'ASA256','2022-08-17 05:16:00',NULL,NULL,'2022-08-17 05:16:00','2022-08-17 11:10:00',25,189,'1820.00','1563.00',7986,36,27),
	(69,20,'ADS881','2022-10-19 06:43:00',NULL,NULL,'2022-10-19 06:43:00','2022-10-19 19:16:00',29,152,'1862.00','1598.00',7495,34,40),
	(70,5,'UED256','2021-05-06 03:38:00',NULL,NULL,'2021-05-06 03:38:00','2021-05-06 04:00:00',8,162,'896.00','682.00',850,15,25),
	(71,7,'CFE269','2021-05-06 04:40:00',NULL,NULL,'2021-05-06 04:40:00','2021-05-06 09:29:00',26,186,'1359.00','983.00',8562,11,14),
	(72,5,'YFS125','2022-06-23 02:53:00',NULL,NULL,'2022-06-23 02:53:00','2022-06-23 11:22:00',26,156,'1285.00','895.00',8546,23,8),
	(73,20,'FGU562','2022-08-19 10:27:00',NULL,NULL,'2022-08-19 10:27:00','2022-08-19 23:30:00',28,168,'1573.00','954.00',9832,18,22),
	(74,8,'YGD250','2022-05-06 09:23:00',NULL,NULL,'2022-05-06 09:23:00','2022-05-06 14:29:00',28,188,'986.00','752.00',6954,12,14),
	(75,1,'F300','2021-05-06 21:00:00',NULL,NULL,'2021-05-06 21:00:00','2021-05-06 22:00:00',48,20,'500.00','1200.00',300,12,18),
	(76,2,'DDF265','2021-05-06 23:15:00',NULL,NULL,'2021-05-06 23:15:00','2021-05-07 02:29:00',28,124,'865.00','574.00',8546,14,19),
	(77,2,'GHT542','2022-05-24 05:15:00',NULL,NULL,'2022-05-24 05:15:00','2022-05-24 10:29:00',28,158,'988.00','687.00',5892,11,26),
	(78,6,'FRJ596','2022-05-18 01:20:00',NULL,NULL,'2022-05-18 01:20:00','2022-05-18 10:47:00',28,126,'857.00','685.00',8592,8,25);

/*Data for the table mileage */

insert  into mileage
	(mileage_id,flight_id,first_mileage,economy_mileage,create_date,update_date) 
values 
	(15,12,2222,12312,'2021-04-27 15:53:35','2021-04-27 15:53:35'),
	(16,15,323,2342,'2021-04-27 15:54:36','2021-04-27 15:54:36'),
	(17,23,674,1233,'2021-04-30 14:43:45','2021-04-30 14:43:45'),
	(18,32,520,530,'2021-05-05 18:30:09','2021-05-05 18:30:09'),
	(19,24,596,756,'2021-05-05 18:32:37','2021-05-05 18:32:37'),
	(20,25,796,986,'2021-05-05 18:32:45','2021-05-05 18:32:45'),
	(21,26,1106,1398,'2021-05-05 18:32:53','2021-05-05 18:32:53'),
	(22,29,1358,1498,'2021-05-05 18:33:02','2021-05-05 18:33:02'),
	(23,33,7652,9896,'2021-05-05 18:33:15','2021-05-05 18:33:15'),
	(24,34,6853,8952,'2021-05-05 18:33:24','2021-05-05 18:33:24'),
	(25,36,397,596,'2021-05-05 18:33:32','2021-05-05 18:33:32'),
	(26,37,652,789,'2021-05-05 18:33:39','2021-05-05 18:33:39'),
	(27,38,460,506,'2021-05-05 18:33:49','2021-05-05 18:33:49'),
	(28,39,456,569,'2021-05-05 18:33:58','2021-05-05 18:33:58'),
	(29,40,259,365,'2021-05-05 18:34:06','2021-05-05 18:34:06'),
	(30,41,295,359,'2021-05-05 18:34:14','2021-05-05 18:34:14'),
	(31,42,6752,8951,'2021-05-05 18:34:23','2021-05-05 18:34:23'),
	(32,43,6899,8958,'2021-05-05 18:34:31','2021-05-05 18:34:31'),
	(33,44,395,489,'2021-05-05 18:34:38','2021-05-05 18:34:38'),
	(34,45,245,385,'2021-05-05 18:34:49','2021-05-05 18:34:49'),
	(35,46,350,395,'2021-05-05 18:34:58','2021-05-05 18:34:58'),
	(36,47,395,456,'2021-05-05 18:35:06','2021-05-05 18:35:06'),
	(37,48,7562,8652,'2021-05-05 18:35:13','2021-05-05 18:35:13'),
	(38,60,6520,7969,'2021-05-05 18:35:24','2021-05-05 18:35:24'),
	(39,58,1986,2165,'2021-05-05 18:35:31','2021-05-05 18:35:31'),
	(40,57,6851,7514,'2021-05-05 18:35:42','2021-05-05 18:35:42'),
	(41,56,5925,6382,'2021-05-05 18:35:52','2021-05-05 18:35:52'),
	(42,55,5894,6985,'2021-05-05 18:36:01','2021-05-05 18:36:01'),
	(43,54,7125,8951,'2021-05-05 18:36:11','2021-05-05 18:36:11'),
	(44,52,7526,8952,'2021-05-05 18:36:24','2021-05-05 18:36:24'),
	(45,53,6483,7521,'2021-05-05 18:36:30','2021-05-05 18:36:30'),
	(46,50,9877,11186,'2021-05-05 18:36:38','2021-05-05 18:36:38'),
	(47,49,93,158,'2021-05-05 18:36:44','2021-05-05 18:36:44'),
	(48,69,5716,6451,'2021-05-05 18:36:56','2021-05-05 18:36:56'),
	(49,68,5923,6854,'2021-05-05 18:37:04','2021-05-05 18:37:04'),
	(50,67,7212,8754,'2021-05-05 18:37:11','2021-05-05 18:37:11'),
	(51,66,5673,6841,'2021-05-05 18:37:21','2021-05-05 18:37:21'),
	(52,65,7214,8472,'2021-05-05 18:37:28','2021-05-05 18:37:28'),
	(53,64,4893,5864,'2021-05-05 18:37:35','2021-05-05 18:37:35'),
	(54,63,5644,6842,'2021-05-05 18:37:41','2021-05-05 18:37:41'),
	(55,62,3974,4562,'2021-05-05 18:37:49','2021-05-05 18:37:49'),
	(56,61,1587,2854,'2021-05-05 18:37:54','2021-05-05 18:37:54'),
	(57,72,6824,7863,'2021-05-06 15:04:11','2021-05-06 15:04:11'),
	(58,73,7542,8752,'2021-05-06 15:04:21','2021-05-06 15:04:21'),
	(59,75,156,256,'2021-05-06 16:02:01','2021-05-06 16:02:01'),
	(60,31,562,752,'2021-05-06 16:02:13','2021-05-06 16:02:13'),
	(61,74,3684,5621,'2021-05-06 16:02:31','2021-05-06 16:02:31'),
	(62,76,3698,5862,'2021-05-06 16:06:45','2021-05-06 16:06:45'),
	(63,77,3541,4352,'2021-05-07 11:06:24','2021-05-07 11:06:24');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
