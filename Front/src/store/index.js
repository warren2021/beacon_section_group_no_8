import { createStore } from 'vuex';

export default createStore({
  state: {
    userInfo: JSON.parse(sessionStorage.getItem('userInfo')||'{}')
  },
  mutations: {
    SET_USERINFO(state, data) {
      state.userInfo = data;
    },
    UPDATE_USERINFO_MSG(state, data) {
      state.userInfo.msg = data;
    }
  },
  actions: {
    setUserInfo({ commit }, data) {
      commit('SET_USERINFO', data);
    }
  },
  getters: {}
})