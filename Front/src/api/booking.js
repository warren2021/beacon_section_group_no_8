import fetch from '/@/service/fetch.js';

export default {
  createOrder: data => fetch({
    url: '/booking/create',
    data
  }),
  queryOders: params => fetch({
    url: '/booking/query',
    method: 'get',
    params
  }),
  refundOrder: params => fetch({
    url: '/booking/refund',
    method: 'get',
    params
  })
}