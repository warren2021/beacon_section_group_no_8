import { formatDate } from '../utils';
import fetch from '/@/service/fetch';

export default {
  getRecomList: params => fetch({
    url: '/flight/defaultList',
    method: 'get',
    params
  }),
  queryFlights: params => {
    params.startDate = formatDate(params.startDate,'YYYY-MM-dd hh:mm:ss')
    params.backDate = formatDate(params.backDate,'YYYY-MM-dd hh:mm:ss')
    return fetch({
    url: '/flight/queryAll',
    method: 'get',
    params
  })},
  getFlightList: params => {
    params.startDate = formatDate(params.startDate,'YYYY-MM-dd hh:mm:ss')
    params.backDate = formatDate(params.backDate,'YYYY-MM-dd hh:mm:ss')
    return fetch({
    url: '/flight/queryList',
    method: 'get',
    params
  })},
  addFlight: data => fetch({
    url: '/flight/add',
    data
  }),
  editFlight: data => fetch({
    url: '/flight/update',
    data
  }),
  delFlight: params => fetch({
    url: '/flight/delete',
    params
  }),
  selectFlight: params => fetch({
    url: '/flight/selectOne',
    method: 'get',
    params
  }),
  getHomeFlight: params => fetch({
    url: '/flight/getHomeList',
    method: 'get',
    params
  })
};
