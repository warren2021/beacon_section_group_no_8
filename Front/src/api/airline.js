import fetch from '/@/service/fetch.js';

export default {
  getAirlineTotal: _ => fetch({
    url: '/airline/list',
    method: 'get'
  }),
  getAirlineList: params => fetch({
    url: '/airline/query',
    method: 'get',
    params
  }),
  addAirline: data => fetch({
    url: '/airline/add',
    data
  }),
  editAirline: data => fetch({
    url: '/airline/update',
    data
  }),
  delAirline: data => fetch({
    url: '/airline/delete',
    data
  })
}