import * as login from './login';
import * as airport from './airport';
import airline from './airline';
import user from './user';
import flight from './flight';
import booking from './booking';
import mileage from './mileage';

const apis = Object.assign({}, login, airport, airline, user, flight, booking, mileage);

export default apis;