import fetch from '/@/service/fetch.js';
const baseUrl = '/airport/'

export const QUERY_AIRPORT_LIST = params => fetch({
  url: `${baseUrl}query`,
  method: 'get',
  params
});

export const INSERT_AIRPORT = params => fetch({
  url: `${baseUrl}add`,
  method: 'post',
  data: params
})

export const DROP_AIRPORT = params => fetch({
  url: `${baseUrl}delete`,
  method: 'post',
  params
})
export const EDIT_AIRPORT = params => fetch({
  url: `${baseUrl}update`,
  method: 'post',
  data:params
})