import fetch from '/@/service/fetch';
const BASE_URL = '/mileage/'

export default {
  INSERT_MILEAGE: data => fetch({
    url: BASE_URL + 'add',
    data
  }),
  DROP_MILEAGE: params => fetch({
    url: BASE_URL + 'delete',
    params
  }),
  UPDATE_MILEAGE: data => fetch({
    url: BASE_URL + 'update',
    data
  }),
  QUERY_MILEAGE_SINGLE: params => fetch({
    url: BASE_URL + 'getById',
    params
  }),
  QUERY_MILEAGE: params => fetch({
    url: BASE_URL + 'getList',
    method: 'get',
    params
  })
};