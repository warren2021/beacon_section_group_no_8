import fetch from '/@/service/fetch.js';

export const login = (params) => fetch({
  url: '/customer/login',
  method: 'post',
  params
});

export const register = params => fetch({
  url: '/customer/register',
  method: 'post',
  params
});

export const checkEamil = params => fetch({
  url: '/customer/check',
  params
})

export const updateUserInfo = params =>fetch({
  url: '/customer/update',
  method:'post',
  data:params
})

export const updateUserPassword = params =>fetch({
  url: '/customer/updatePwd',
  method:'post',
  params
})