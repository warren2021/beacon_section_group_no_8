import fetch from '/@/service/fetch.js';
const BASE_ADDRESS_URL = '/address/'
const BASE_CRD_URL = '/creditCard/'
const BASE_EC_URL = '/certificate/'
export default {
  QUERY_ADDRESS: params => fetch({
    url: BASE_ADDRESS_URL + 'select',
    method: 'get',
    params
  }),
  INSERT_ADDRESS: data => fetch({
    url: BASE_ADDRESS_URL + 'add',
    data
  }),
  UPDATE_ADDRESS: data => fetch({
    url: BASE_ADDRESS_URL + 'update',
    data
  }),
  DROP_ADDRESS: params => fetch({
    url: BASE_ADDRESS_URL + 'delete',
    params
  }),
  QUERY_USER_LIST:params => fetch({
    url:'/customer/getList',
    method:'get',
    params
  }),
  QUERY_CREDIT_CARD: (params = {}) => fetch({
    url: BASE_CRD_URL + 'queryByCustomer',
    method: 'get',
    params,
  }),
  INSERT_CREDIT_CARD: (data = {}) => fetch({
    url: BASE_CRD_URL + 'add',
    params: {
      addressId: data.addressId
    },
    data
  }),
  UPDATE_CREDIT_CARD: data => fetch({
    url: BASE_CRD_URL + 'update',
    params: {
      addressId: data.addressId
    },
    data,
  }),
  DROP_CREDIT_CARD: params => fetch({
    url: BASE_CRD_URL + 'delete',
    params
  }),
  QUERY_ORDER_SELF: params => fetch({
    url: '/booking/query',
    method: 'get',
    params
  }),
  QUERY_ORDER_SINGLE: params => fetch({
    url: '/booking/queryById',
    method: 'get',
    params
  }),
  REFUND_TICKET: params => fetch({
    url: '/booking/refund',
    method: 'get',
    params
  }),
  INSERT_EC:params=>fetch({
    url:BASE_EC_URL+'add',
    params
  }),
  QUERY_EC:params=>fetch({
    url:BASE_EC_URL+'query',
    method:'get',
    params
  }),
  UPDATE_EC:data=>fetch({
    url:BASE_EC_URL +'update',
    data
  }),
  DROP_EC:params=>fetch({
    url:BASE_EC_URL+'delete',
    params
  })
}