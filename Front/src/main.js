import { createApp } from 'vue'
import App from './App.vue'

import router from './router';
import store from './store';

import ElementPlus from "element-plus";
import "element-plus/lib/theme-chalk/index.css";

import globalCpm from '/@/components/global';

import apis from './api';

import './style/index.scss'

const app = createApp(App)
new globalCpm(app)
app.use(ElementPlus)
  .provide('http', apis)
  .use(store)
  .use(router)
  .mount('#app')
 
