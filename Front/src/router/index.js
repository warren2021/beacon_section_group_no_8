import {
  createRouter,
  createWebHashHistory
} from 'vue-router'

const router = createRouter({
  history: createWebHashHistory(),
  routes: [{
      path: '/',
      redirect: {
        name: 'Home'
      },
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('/@/views/login/index.vue'),
      meta: {
        index: 1
      }
    },
    {
      path: '/register',
      name: 'register',
      component: () => import('/@/views/login/register.vue'),
      meta: {
        index: 1
      }
    },
    {
      path: '/user',
      name: 'user',
      component: () => import('/@/views/user/index.vue'),
      meta: {
        index: 1
      },
    },
    {
      path: '/order/:id',
      name: 'orderInfo',
      component: () => import('/@/views/order/orderInfo.vue'),
      meta: {
        index: 1
      }
    },
    {
      path: '/control',
      redirect:'/control/user'
    },
    {
      path: '/control/:type',
      name: 'Control',
      component: () => import('/@/views/control/index.vue'),
      meta: {}
    },
    {
      path: '/home',
      name: 'Home',
      component: () => import('/@/views/flight/home.vue'),
      meta: {}
    },
    {
      path: '/booking',
      name: 'Booking',
      component: () => import('../views/flight/booking.vue'),
      meta: { index: 1 }
    },
    {
      path: '/flight',
      name: 'Flight',
      component: () => import('../views/flight/index.vue'),
      meta: { index: 1 }
    },
    {
      path: '/reserve',
      name: 'Reserve',
      component: () => import('../views/flight/reserve.vue'),
      meta: { index: 1 }
    },
    {
      path: '/flight/reserve',
      name: 'FlightReserve',
      component: () => import('../views/flight/reserve.vue'),
    },
  ]
})

export default router