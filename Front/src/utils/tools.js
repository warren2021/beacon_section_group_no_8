import { ElMessage, ElMessageBox } from 'element-plus';
import router from '/@/router';
import store from '/@/store';
export default {
  routerPush(path) {
    router.push(path)
  },
  routerReplace(path) {
    router.replace(path)
  },
  getUserInfo() {
    let u = null
    try {
      u = JSON.parse(sessionStorage.userInfo)
    } catch (error) {
      ElMessage.info('Not logged in, please login and then operate!')
      router.push('/login')
    }
    return u||{}
  },
  formatBankCard(str = '', separator = ' ') {
    str = str.toString();
    var temp = str.length % 4;
    if (temp > 0) {
      return str.match(/\d{4}/g).join(separator) + separator + str.substr(-temp, temp);
    } else {
      return str.match(/\d{4}/g).join(separator);
    }
  },
  plusXing(str, frontLen, endLen) {
    var len = str.length - frontLen - endLen;
    var xing = '';
    for (var i = 0; i < len; i++) {
      xing += '*';
    }
    return str.substring(0, frontLen) + xing + str.substring(str.length - endLen);
  },
  timeDifference(start, end, type = { d: 'd', h: 'h', m: 'm' }) {
    if (end - start) return '';
    start = new Date(start).getTime();
    end = new Date(end).getTime();
    const timeDiff = start - end;
    let str = '';
    const days = Math.floor(timeDiff / (24 * 3600 * 1000));
    str += days ? `${days}${type.d}` : '';
    const leave1 = timeDiff % (24 * 3600 * 1000);
    const hours = Math.floor(leave1 / (3600 * 1000));
    str += hours ? `${hours}${type.h}` : '';
    const leave2 = leave1 % (3600 * 1000);
    const minutes = Math.floor(leave2 / (60 * 1000));
    str += minutes ? `${minutes}${type.m}` : '';
    return str;
  },
  logout(){
    const app = document.getElementById('app')
    app.classList.add('gray')
    return ElMessageBox.confirm('confirm exit？','Hint',{
      confirmButtonText: 'confirm',
      cancelButtonText: 'cancel',
      beforeClose: (a, i, done) => {
        app.classList.remove('gray');
        done();
      },
      type: 'warning'
    }).then(()=>{
      store.dispatch("setUserInfo", {});
      sessionStorage.userInfo = "";
      const curRouterName = router.currentRoute.value.name
      console.log('[  ] >', curRouterName)
      console.log( !['Home','Booking','Flight'].includes(curRouterName))
      if(!['Home','Booking','Flight'].includes(curRouterName)) router.replace('/home')
    }).catch(_ => {})
  },
  confirmDialog(fun,params, cbk) {
    ElMessageBox.confirm("Confirm that the information is deleted?", "Hint", {
      confirmButtonText: "delete",
      cancelButtonText: "cancel",
      type: "warning"
    }).then(async () => {
      await fun(params)
      ElMessage({
        type: "success",
        duration: 800,
        message: "Successful operation!",
        onClose() {
          cbk()
        }
      });
    }).catch(_ => {})
  }
}