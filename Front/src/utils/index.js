import tools from './tools';

function formatDate(date, format = 'yyyy-MM-dd') {
  if (!date) return '';
  date = new Date(date);
  let result = format;
  const time = {
    'M+': date.getMonth() + 1,
    'd+': date.getDate(),
    'h+': date.getHours(),
    'm+': date.getMinutes(),
    's+': date.getSeconds(),
    'q+': Math.floor((date.getMonth() + 3) / 3),
    'S+': date.getMilliseconds(),
  };
  if (/(y+)/i.test(result)) {
    result = result.replace(RegExp.$1, (`${date.getFullYear()}`).substr(4 - RegExp.$1.length));
  }
  Object.keys(time).forEach((k) => {
    if (new RegExp(`(${k})`).test(result)) {
      result = result.replace(RegExp.$1, RegExp.$1.length === 1 ? time[k] : (`00${time[k]}`).substr((`${time[k]}`).length));
    }
  });
  return result;
}

export {
  tools,
  formatDate
}