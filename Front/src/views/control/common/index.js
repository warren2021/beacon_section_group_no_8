export { default as user } from './user.vue';
export { default as flight } from './flight.vue';
export { default as airline } from './airline.vue';
export { default as airport } from './airport.vue';
export { default as mileage } from './mileage.vue';
export { default as orderInfo } from './orderInfo.vue';
