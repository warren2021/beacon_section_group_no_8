import rail from './rail.vue';
import table from './table/index.vue';
import pagination from './pagination/index.vue';

const components = [
  rail,
  table,
  pagination
]

class RegisterComp {
  constructor(app) {
    this.app = app
    this.__init()
  }
  __init() {
    components.forEach(cmp => {
      this.app.component(cmp.name, cmp);
    })
  }
}

export default RegisterComp