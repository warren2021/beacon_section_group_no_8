import axios from "axios";
import { ElMessage } from "element-plus";

const service = axios.create({
  baseURL: "/hps",
  method: 'post',
  timeout: 300000
});

service.interceptors.request.use(
  config => {
    config.headers.Authorization = "";
    config.withCredentials = false;
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

service.interceptors.response.use(
  response => response,
  error => {
    return Promise.resolve(error.response);
  }
);

const fetch = config => {
  return new Promise((resolve, reject) => {
    service(config)
      .then(response => checkStatus(response))
      .then(result => {
        const { code, status } = result;
        if (code === 200) {
          resolve(result.data);
        } else {
          ElMessage.error(result.message);
          reject(result);
        }
      })
      .catch(error => {
        reject(error);
      });
  });
};

function checkStatus(response) {
  if (response && (response.status === 200 || response.status === 304)) {
    return response.data;
  }
  if (!response) {
    return { status: -1, message: "Unable to connect the server, please try again later" };
  }
  return { status: response.status, message: `Server returns an error` };
}

export default fetch;
