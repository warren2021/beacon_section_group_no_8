const path = require('path')
module.exports = {
  alias: {
    '/@/': path.resolve(__dirname, './src')
  }, 
  optimizeDeps: {
    include: ["element-plus/lib/locale/lang/zh-cn", "dayjs/locale/zh-cn"],
  },
  proxy: {
    '/hps': {
      target: 'http://192.168.111.222:19090/',
      changeOrigin: true,
    }
  }
}
