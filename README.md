In the process of set up this system, front-end adopted a Vue, back-end Springboot + Druid and USES the MYSQL database technology; The function of this system is realized by using these technologies.

Through the above technology, the system has achieved the following functions:
	User Function Points:
a)3.1  User login and registration
b)3.2  Ticket query
c)3.3  Flight information
d)3.4  Personal Information Maintenance
e)3.5  Unbind personal credit card
f)3.6  Order book
g)3.7  Order payment
h)3.8  Travel query

	Administrator Function Points:
a)3.1  User information maintenance
b)3.2  Flight information maintenance
c)3.3  Order information maintenance
d)3.4  Airline information maintenance
e)3.5  Airport information maintenance
f)3.6  Mileage information maintenance

System test：
Address: 
Login Address: http://47.93.53.97/#/login 

User Account： 
Account: user@163.com 
Password: 123456 

Manager Account: 
Account: admin@163.com  
Password: 111111 

Note : Don't delete the sample data . You could register the new user account by yourself .



