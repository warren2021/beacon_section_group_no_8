package com.my.hps.scheduler;

import com.my.hps.service.CustomerService;
import com.my.hps.service.FlightService;
import com.my.hps.model.BookingEntity;
import com.my.hps.model.Customer;
import com.my.hps.model.FlightEntity;
import com.my.hps.service.BookingService;
import com.my.hps.utils.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class Scheduler {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private BookingService bookingService;

    @Autowired
    private FlightService flightService;

    @Autowired
    private CustomerService customerService;

    @Scheduled(cron = "0 */1 * * * ?")
    public void checkBooking() {
        int pageNum = 1;
        int pageSize = 50;
        Query query = new Query();
        while (true) {
            query.setPageNum(pageNum++).setPageSize(pageSize);
            Map<String, Object> map = (Map<String, Object>) bookingService.query(null, 1, query).getData();
            List<BookingEntity> result = (List<BookingEntity>) map.get("list");
            if (result == null || result.size() == 0) {
                break;
            }
            for (int i = 0; i < result.size(); i++) {
                BookingEntity bookingEntity = result.get(i);
                FlightEntity flightEntity = flightService.selectOne(bookingEntity.getFlightId());
                long now = System.currentTimeMillis();
                if (flightEntity.getArriveDate().getTime() <= now && bookingEntity.getState() == 1) {
                    log.info("booking：" + bookingEntity.getBookingNumber() + "END！");
                    bookingService.update(bookingEntity.setState(2));
                    Customer customer = customerService.selectOne(bookingEntity.getCustomerId());
                    customerService.update(new Customer()
                            .setCustomerId(bookingEntity.getCustomerId())
                            .setMileageNum(customer.getMileageNum()+bookingEntity.getMileageSum()));
                }
            }
        }
    }

}
