package com.my.hps.service;

import com.my.hps.model.CreditCard;
import com.my.hps.utils.Result;

public interface CreditCardService {
    Result add(CreditCard creditCard,Long addressId);

    Result update(CreditCard creditCard,Long addressId);

    Result delete(Long creditCardId);

    Result queryByCustomer(Long customerId);

    CreditCard getById(Long creditCardId);
}
