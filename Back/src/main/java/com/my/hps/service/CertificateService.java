package com.my.hps.service;

import com.my.hps.model.CertificateEntity;
import com.my.hps.utils.Result;

import java.util.List;

public interface CertificateService {
    Result query(Long customerId);

    Result update(CertificateEntity certificateEntity);

    Result delete(Long certificateId);

    Result add(CertificateEntity creditCard);

    List<CertificateEntity> queryByIds(Object[] arr);
}
