package com.my.hps.service.impl;


import com.my.hps.service.FlightService;
import com.my.hps.mapper.FlightMapper;
import com.my.hps.model.FlightEntity;
import com.my.hps.utils.Query;
import com.my.hps.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FlightServiceImpl implements FlightService {

    @Autowired
    private FlightMapper mapper;

    @Override
    public Result update(FlightEntity flightEntity) {
        long startTime = flightEntity.getFlightDate().getTime();
        long arriveTime = flightEntity.getArriveDate().getTime();

        if (startTime > arriveTime) {
            return new Result().setCode(1000).setMessage("Take off time cannot be less than landing time");
        }
        mapper.update(flightEntity);
        return new Result().setCode(200);

    }

    @Override
    public Result delete(Long flightId) {
        mapper.delete(flightId);
        return new Result().setCode(200);

    }

    @Override
    public Result add(FlightEntity flightEntity) {

        long startTime = flightEntity.getFlightDate().getTime();
        long arriveTime = flightEntity.getArriveDate().getTime();

        if (startTime > arriveTime) {
            return new Result().setCode(1000).setMessage("Take off time cannot be less than landing time");
        }
        mapper.add(flightEntity);
        return new Result().setCode(200);
    }

    @Override
    public Result queryList(FlightEntity flightEntity, Query query) {
        List<FlightEntity> list = mapper.queryList(flightEntity, query);
        Integer total = mapper.queryListCount(flightEntity);

        Map<String, Object> map = new HashMap<>();
        map.put("total", total);
        map.put("list", list);
        return new Result().setData(map).setCode(200);
    }

    @Override
    public FlightEntity selectOne(Long flightId) {
        return mapper.selectOne(flightId);
    }

    @Override
    public Result defaultList() {
        List<FlightEntity> list = mapper.defaultList();
        Integer total = mapper.defaultListCount();
        Map<String, Object> map = new HashMap<>();
        map.put("total", total);
        map.put("list", list);
        return new Result().setData(map).setCode(200);
    }

    @Override
    public Object getHomeList() {
        Map<String, Object> map = new HashMap<>();
        map.put("domestic", mapper.getDomestic());
        map.put("notDomestic", mapper.getNotDomestic());
        return new Result<>().setData(map);
    }

    @Override
    public Result queryAll(FlightEntity flightEntity, Query query) {
        List<FlightEntity> list = mapper.queryAll(flightEntity, query);
        Integer total = mapper.queryAllCount(flightEntity);
        Map<String, Object> map = new HashMap<>();
        map.put("total", total);
        map.put("list", list);
        return new Result().setData(map).setCode(200);
    }
}
