package com.my.hps.service;

import com.my.hps.model.AddressEntity;
import com.my.hps.utils.Result;

public interface AddressService {
    Result add(AddressEntity addressEntity);

    Result update(AddressEntity addressEntity);

    Result delete(AddressEntity setAddressID);

    Result select(AddressEntity setCustomerId);

    AddressEntity selectOne(AddressEntity setAddressID);
}
