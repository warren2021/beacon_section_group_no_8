package com.my.hps.service;

import com.my.hps.model.Customer;
import com.my.hps.utils.Query;
import com.my.hps.utils.Result;

import java.util.Map;

public interface CustomerService {
    Customer selectByEmail(Customer customer);

    Result register(Customer customer);

    Result login(Customer customer);

    Result update(Customer customer);

    Result check(String email);

    Result updatePwd(String pwd, Integer type, String newPwd,String email);

    Customer selectOne(Long customerId);

    Map<String, Object> getList(Customer customer, Query query);
}
