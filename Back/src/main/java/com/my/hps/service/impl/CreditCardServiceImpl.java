package com.my.hps.service.impl;

import com.my.hps.service.AddressService;
import com.my.hps.service.CreditCardService;
import com.my.hps.mapper.CreditCardMapper;
import com.my.hps.model.AddressEntity;
import com.my.hps.model.CreditCard;
import com.my.hps.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CreditCardServiceImpl implements CreditCardService {

    @Autowired
    private CreditCardMapper mapper;

    @Autowired
    private AddressService addressService;

    @Override
    public Result add(CreditCard creditCard, Long addressId) {

        Long creditCardId = mapper.add(creditCard);

        AddressEntity addressEntity = addressService.selectOne(new AddressEntity().setAddressId(addressId));
        CreditCard result = this.getById(addressEntity.getCreditCardId());
        if (addressEntity.getCreditCardId() == null || addressEntity.getCreditCardId() == 0 || result == null) {
            addressService.update(addressEntity.setCreditCardId(creditCardId));
            return new Result().setCode(200);
        }
        if (addressEntity.getCreditCardId() == creditCard.getCreditCardId()) {
            return new Result().setCode(200);
        }
        return new Result().setCode(1000).setMessage("The address ID is already in use");
    }

    @Override
    @Transactional
    public Result update(CreditCard creditCard, Long addressId) {

        mapper.update(creditCard);
        AddressEntity addressEntity = addressService.selectOne(new AddressEntity().setAddressId(addressId));
        CreditCard result = this.getById(addressEntity.getCreditCardId());
        if (addressEntity.getCreditCardId() == null || addressEntity.getCreditCardId() == 0 || result == null) {
            addressService.update(addressEntity.setCreditCardId(creditCard.getCreditCardId()));
            return new Result().setCode(200);
        }
        if (addressEntity.getCreditCardId() == creditCard.getCreditCardId()) {
            return new Result().setCode(200);
        }
        return new Result().setCode(1000).setMessage("The address ID is already in use");
    }

    @Override
    public Result delete(Long creditCardId) {
        mapper.delete(creditCardId);
        return new Result().setCode(200);
    }

    @Override
    public Result queryByCustomer(Long customerId) {
        List<CreditCard> list = mapper.queryByCustomer(customerId);
        return new Result().setCode(200).setData(list);
    }

    @Override
    public CreditCard getById(Long creditCardId) {
        return mapper.getById(creditCardId);
    }


}
