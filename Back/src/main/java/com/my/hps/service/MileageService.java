package com.my.hps.service;

import com.my.hps.model.MileageEntity;
import com.my.hps.utils.Query;

import java.util.Map;

public interface MileageService {
    void add(MileageEntity param);

    Map<String, Object> getList(MileageEntity param, Query query);

    void update(MileageEntity param);

    void delete(Long mileageId);

    MileageEntity getById(Long mileageId);

    MileageEntity selectOne(Long fightId);
}
