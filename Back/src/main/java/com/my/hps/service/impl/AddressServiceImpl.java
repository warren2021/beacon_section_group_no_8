package com.my.hps.service.impl;

import com.my.hps.service.AddressService;
import com.my.hps.service.CreditCardService;
import com.my.hps.mapper.AddressMapper;
import com.my.hps.model.AddressEntity;
import com.my.hps.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AddressServiceImpl  implements AddressService {

    @Autowired
    private AddressMapper mapper;

    @Autowired
    private CreditCardService creditCardService;
    @Override
    public Result add(AddressEntity addressEntity) {
        mapper.add(addressEntity);
        return new Result().setCode(200);
    }

    @Override
    public Result update(AddressEntity addressEntity) {
        mapper.update(addressEntity);
        return new Result().setCode(200);
    }

    @Override
    public Result delete(AddressEntity setAddressID) {
       AddressEntity addressEntity= mapper.selectOne(setAddressID.getAddressId());
        if(addressEntity.getCreditCardId() == null || addressEntity.getCreditCardId() ==null){
            mapper.delete(setAddressID.getAddressId());
            return new Result().setCode(200);
        }
        return new Result().setCode(1000).setMessage("The address is already bound with a credit card");
    }

    @Override
    public Result select(AddressEntity setCustomerId) {
        List<AddressEntity> list= mapper.select(setCustomerId.getCustomerId());

        return new Result().setData(list).setCode(200);
    }

    @Override
    public AddressEntity selectOne(AddressEntity setAddressID) {

        return mapper.selectOne(setAddressID.getAddressId());


    }
}
