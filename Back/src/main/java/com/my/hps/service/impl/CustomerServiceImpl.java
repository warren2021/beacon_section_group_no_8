package com.my.hps.service.impl;

import com.my.hps.service.BookingService;
import com.my.hps.service.CustomerService;
import com.my.hps.mapper.CustomerMapper;
import com.my.hps.model.Customer;
import com.my.hps.utils.Md5Util;
import com.my.hps.utils.Query;
import com.my.hps.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerMapper mapper;

    @Autowired
    private BookingService bookingService;

    @Override
    public Customer selectByEmail(Customer customer) {

        return mapper.selectByEmail(customer);
    }

    @Override
    public Result register(Customer customer) {
        if(mapper.selectByEmail(customer) == null ){
            mapper.register(customer);
            return  new Result().setCode(200);
        }else {
            return  new Result().setCode(1000).setMessage("Repeat registration");
        }
    }

    @Override
    public Result login(Customer customer) {

      Customer  result= mapper.selectByEmail(customer);
      if(result != null ){
          if(customer.getPassword().equals(result.getPassword()) && result.getIdentity().equals(customer.getIdentity())){
             Long  countMileageSum=bookingService.selectByCust(result.getCustomerId());
              result.setPayPassword(null).setPassword(null).setCountMileageSum(countMileageSum);
              return  new Result().setData(result).setCode(200);
          }
          return  new Result().setCode(1000).setMessage("Wrong account or password");
      }
       return  new Result().setCode(1000).setMessage("Wrong account or password");
    }

    @Override
    public Result update(Customer customer) {
        if(customer.getPassword() != null ){
            customer.setPassword(Md5Util.getMD5Str(customer.getPassword()));
        }
        if(customer.getPayPassword() != null ){
            customer.setPayPassword(Md5Util.getMD5Str(customer.getPayPassword()));
        }
        mapper.update(customer);
        return   new Result().setCode(200);
    }

    @Override
    public Result check(String email) {
        if(mapper.selectByEmail(new Customer().setEmail(email)) == null ){
            return  new Result().setCode(200);
        }else {
            return  new Result().setCode(1000).setMessage("Registered");
        }
    }

    @Override
    public Result updatePwd(String pwd, Integer type, String newPwd,String email) {
        Customer  result= mapper.selectByEmail(new Customer().setEmail(email));
        if(type == 1){
            if(Md5Util.getMD5Str(pwd).equals(result.getPassword())){
                result.setPassword(Md5Util.getMD5Str(newPwd));
                mapper.update(result);
                return new Result().setCode(200);
            }
            return  new Result().setCode(1000).setMessage("Original password error");
        }else if(type == 2){
            if(Md5Util.getMD5Str(pwd).equals(result.getPassword())){
                result.setPayPassword(Md5Util.getMD5Str(newPwd));
                mapper.update(result);
                return new Result().setCode(200);
            }
            return  new Result().setCode(1000).setMessage("Original password error");
        }

        return  new Result().setCode(1000).setMessage("Parameter error");
    }

    @Override
    public Customer selectOne(Long customerId) {

        return mapper.selectOne(customerId);
    }

    @Override
    public Map<String, Object> getList(Customer customer, Query query) {
        List<Customer> list = mapper.getList(customer,query);
        Integer total = mapper.queryListCount(customer);
        Map<String, Object> map = new HashMap<>();
        map.put("total", total);
        map.put("list", list);
        return map;
    }


}
