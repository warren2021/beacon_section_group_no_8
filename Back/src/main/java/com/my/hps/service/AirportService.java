package com.my.hps.service;

import com.my.hps.model.AirportEntity;
import com.my.hps.utils.Query;
import com.my.hps.utils.Result;

public interface AirportService {
    Result add(AirportEntity airportEntity);

    Result delete(Long airportId);

    Result update(AirportEntity airportEntity);

    Result query(AirportEntity airportEntity, Query query);
}
