package com.my.hps.service.impl;

import com.alibaba.fastjson.JSON;
import com.my.hps.mapper.BookingMapper;
import com.my.hps.service.*;
import com.my.hps.model.BookingEntity;
import com.my.hps.model.Customer;
import com.my.hps.model.FlightEntity;
import com.my.hps.model.MileageEntity;
import com.my.hps.utils.Query;
import com.my.hps.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BookingServiceImpl implements BookingService {
    @Autowired
    private BookingMapper mapper;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private FlightService flightService;

    @Autowired
    private CertificateService certificateService;

    @Autowired
    private MileageService mileageService;

    @Override
    @Transactional
    public Result create(Long flightId, Long customerId, String certificateIds, Integer seat, Long creditCardId) {
        Customer customer = customerService.selectOne(customerId);
//        if (customer != null && Md5Util.getMD5Str(payPwd).equals(customer.getPayPassword())) {
        FlightEntity flightEntity = flightService.selectOne(flightId);
        if (flightEntity != null) {
            MileageEntity mileageEntity = mileageService.selectOne(flightId);
            BookingEntity bookingEntity = new BookingEntity();
            bookingEntity.
                    setCustomerId(customerId).
                    setFlightId(flightId).setSeat(seat).
                    setDepartureAirport(flightEntity.getDepartureAirport()).
                    setDestinationAirport(flightEntity.getDestinationAirport()).
                    setCertificateIds(certificateIds).
                    setDepartureTime(flightEntity.getStartDate()).
                    setDestinationTime(flightEntity.getArriveDate()).
                    setCreditCardId(creditCardId).
                    setBookingNumber("BOK-" + new Date().getTime());

            Object[] arr = JSON.parseArray(certificateIds).toArray();
            synchronized (flightEntity) {
                if (mileageEntity != null) {
                    if (bookingEntity.getSeat() == 1) {
                        bookingEntity.setMileageSum(mileageEntity.getEconomyMileage() * arr.length);
                    } else {
                        bookingEntity.setMileageSum(mileageEntity.getFirstMileage() * arr.length);
                    }
                } else {
                    bookingEntity.setMileageSum(0);
                }
                if (seat == 1) {
                    if (flightEntity.getEconomyClassNum() < arr.length) {
                        return new Result().setCode(1000).setMessage("No ticket");
                    }
                    bookingEntity.setPrice(flightEntity.getFirstClassPrice())
                            .setTicketSum(arr.length)
                            .setPriceSum(flightEntity.getFirstClassPrice() * arr.length);
                    flightService.update(flightEntity.setEconomyClassNum(flightEntity.getEconomyClassNum() - arr.length));
                } else if (seat == 2) {
                    if (flightEntity.getFirstClassNum() < arr.length) {
                        return new Result().setCode(1000).setMessage("No ticket");
                    }
                    flightService.update(flightEntity.setFirstClassNum(flightEntity.getFirstClassNum() - arr.length));
                    bookingEntity.setPrice(flightEntity.getEconomyClassPrice())
                            .setTicketSum(arr.length)
                            .setPriceSum(flightEntity.getEconomyClassPrice() * arr.length);
                }
            }
            mapper.create(bookingEntity);
            return new Result().setCode(200);
        }
        return new Result().setCode(1000).setMessage("Flight does not exist");
    }

    @Override
    public Result query(Long customerId, Integer status, Query query) {
        List<BookingEntity> list = mapper.query(customerId, status, query);
        for (int i = 0; i < list.size(); i++) {
            BookingEntity be = list.get(i);
            if (be == null || be.getCertificateIds() == null) {
                continue;
            }
            Object[] arr = JSON.parseArray(be.getCertificateIds()).toArray();
            be.setCertificateList(certificateService.queryByIds(arr));
        }
        Integer total = mapper.queryCount(customerId, status);
        Map<String, Object> map = new HashMap<>();
        map.put("total", total);
        map.put("list", list);
        return new Result().setData(map);
    }

    @Override
    public Result refund(Long bookId) {
        BookingEntity bookingEntity = mapper.selectOne(bookId);
        if (bookingEntity != null) {
            if(bookingEntity.getState() == 2){
                return new Result().setCode(1000).setMessage("The trip is over");
            }
            FlightEntity flightEntity = flightService.selectOne(bookingEntity.getFlightId());
            if (flightEntity != null) {
                Object[] arr = JSON.parseArray(bookingEntity.getCertificateIds()).toArray();
                if (bookingEntity.getSeat() == 1) {
                    flightService.update(flightEntity.setEconomyClassNum(flightEntity.getEconomyClassNum() + arr.length));
                } else {
                    flightService.update(flightEntity.setFirstClassNum(flightEntity.getFirstClassNum() + arr.length));
                }
            }
            mapper.update(bookingEntity.setState(3));
            return new Result().setCode(200);
        }

        return new Result().setCode(1000).setMessage("non-existent");
    }

    @Override
    public Long selectByCust(Long customerId) {
        return mapper.selectByCust(customerId);
    }

    @Override
    public Result queryById(Long bookingId) {
        BookingEntity result = mapper.selectOne(bookingId);
        if (result == null || result.getCertificateIds() == null) {
            return new Result().setData(result).setCode(200);
        }
        Object[] arr = JSON.parseArray(result.getCertificateIds()).toArray();
        result.setCertificateList(certificateService.queryByIds(arr));
        return new Result().setData(result).setCode(200);
    }

    @Override
    public void update(BookingEntity bookingEntity) {
        mapper.update(bookingEntity);
    }

}
