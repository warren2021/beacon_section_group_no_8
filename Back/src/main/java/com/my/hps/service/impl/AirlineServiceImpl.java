package com.my.hps.service.impl;

import com.my.hps.service.AirlineService;
import com.my.hps.mapper.AirlineMapper;
import com.my.hps.model.AirlineEntity;
import com.my.hps.utils.Query;
import com.my.hps.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AirlineServiceImpl implements AirlineService {

    @Autowired
    private AirlineMapper mapper;

    @Override
    public Result add(AirlineEntity airlineEntity) {
        mapper.add(airlineEntity);
        return new Result().setCode(200);
    }

    @Override
    public Result delete(Long airId) {
        mapper.delete(airId);
        return new Result().setCode(200);
    }

    @Override
    public Result update(AirlineEntity airlineEntity) {
        mapper.update(airlineEntity);
        return new Result().setCode(200);
    }

    @Override
    public Result query(AirlineEntity param, Query query) {
        List<AirlineEntity> list=mapper.query(param,query);
        Integer total = mapper.queryListCount(param);
        Map<String, Object> map = new HashMap<>();
        map.put("total", total);
        map.put("list", list);
        return new Result().setData(map);
    }
}
