package com.my.hps.service.impl;

import com.my.hps.mapper.MileageMapper;
import com.my.hps.model.MileageEntity;
import com.my.hps.service.MileageService;
import com.my.hps.utils.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MileageServiceImpl implements MileageService {

    @Autowired
    private MileageMapper mapper;

    @Override
    public void add(MileageEntity param) {
        mapper.add(param);
    }

    @Override
    public Map<String, Object> getList(MileageEntity param, Query query) {
        List<MileageEntity> list = mapper.getList(param,query);
        Integer total = mapper.queryListCount(param);
        Map<String, Object> map = new HashMap<>();
        map.put("total", total);
        map.put("list", list);
        return map;
    }

    @Override
    public void update(MileageEntity param) {
        mapper.update(param);
    }

    @Override
    public void delete(Long mileageId) {
        mapper.delete(mileageId);
    }

    @Override
    public MileageEntity getById(Long mileageId) {
        return mapper.getById(mileageId);
    }

    @Override
    public MileageEntity selectOne(Long fightId) {
        return mapper.selectOne(fightId);
    }

}
