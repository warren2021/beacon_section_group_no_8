package com.my.hps.service;

import com.my.hps.model.FlightEntity;
import com.my.hps.utils.Query;
import com.my.hps.utils.Result;

public interface FlightService {
    Result update(FlightEntity flightEntity);

    Result delete(Long flightId);

    Result add(FlightEntity flightEntity);

    Result queryList(FlightEntity flightEntity, Query query);

    FlightEntity selectOne(Long flightId);

    Result defaultList();

    Object getHomeList();

    Result queryAll(FlightEntity flightEntity, Query query);
}
