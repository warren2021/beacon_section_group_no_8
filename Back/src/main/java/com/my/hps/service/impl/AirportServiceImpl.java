package com.my.hps.service.impl;

import com.my.hps.service.AirportService;
import com.my.hps.mapper.AirportMapper;
import com.my.hps.model.AirportEntity;
import com.my.hps.utils.Query;
import com.my.hps.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AirportServiceImpl implements AirportService {
    @Autowired
    private AirportMapper mapper;

    @Override
    public Result add(AirportEntity airportEntity) {
        mapper.add(airportEntity);
        return new Result().setCode(200);
    }

    @Override
    public Result delete(Long airportId) {
        mapper.delete(airportId);
        return new Result().setCode(200);
    }

    @Override
    public Result update(AirportEntity airportEntity) {
        mapper.update(airportEntity);
        return new Result().setCode(200);
    }

    @Override
    public Result query(AirportEntity airportEntity, Query query) {
        List<AirportEntity> list = mapper.query(airportEntity,query);
        Integer total = mapper.queryListCount(airportEntity);
        Map<String, Object> map = new HashMap<>();
        map.put("total", total);
        map.put("list", list);
        return new Result().setData(map);
    }
}
