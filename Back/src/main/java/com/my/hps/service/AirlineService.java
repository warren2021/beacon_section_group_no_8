package com.my.hps.service;

import com.my.hps.model.AirlineEntity;
import com.my.hps.utils.Query;
import com.my.hps.utils.Result;

public interface AirlineService {
    Result add(AirlineEntity airlineEntity);

    Result delete(Long airId);

    Result update(AirlineEntity airlineEntity);

    Result query(AirlineEntity param, Query query);
}
