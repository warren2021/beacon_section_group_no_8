package com.my.hps.service;

import com.my.hps.model.BookingEntity;
import com.my.hps.utils.Query;
import com.my.hps.utils.Result;

public interface BookingService {
    Result create(Long flightId, Long customerId, String certificateIds, Integer seat, Long creditCardId);

    Result query(Long customerId, Integer status, Query query);

    Result refund(Long bookId);

    Long selectByCust(Long customerId);

    Result queryById(Long bookingId);

    void update(BookingEntity setState);
}
