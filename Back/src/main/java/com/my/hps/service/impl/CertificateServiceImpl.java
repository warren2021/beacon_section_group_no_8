package com.my.hps.service.impl;

import com.my.hps.service.CertificateService;
import com.my.hps.mapper.CertificateMapper;
import com.my.hps.model.CertificateEntity;
import com.my.hps.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CertificateServiceImpl implements CertificateService {

    @Autowired
    private CertificateMapper mapper;


    @Override
    public Result query(Long customerId) {
        List<CertificateEntity> list=mapper.query( customerId);
        return new Result().setData(list).setCode(200);
    }

    @Override
    public Result update(CertificateEntity certificateEntity) {
        mapper.update(certificateEntity);
        return new Result().setCode(200);
    }

    @Override
    public Result delete(Long certificateId) {
        mapper.delete(certificateId);
        return new Result().setCode(200);
    }

    @Override
    public Result add(CertificateEntity creditCard) {
        mapper.add(creditCard);
        return new Result().setCode(200);
    }

    @Override
    public List<CertificateEntity> queryByIds(Object[] arr) {
        return mapper.queryByIds(arr);
    }
}
