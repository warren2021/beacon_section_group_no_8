package com.my.hps.utils;

public class Query {

    private Integer pageSize;

    private Integer pageNum;

    private String order;

    public Query(Integer pageSize, Integer pageNum, String order) {
        this.pageSize = pageSize;
        this.pageNum = pageNum;
        this.order = order;
    }
    public Query(){

    }

    public Integer getPageSize() {

        return pageSize;
    }

    public Query setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public Integer getPageNum() {
        return pageSize*(pageNum-1);
    }

    public Query setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
        return this;
    }

    public String getOrder() {
        return order;
    }

    public Query setOrder(String order) {
        this.order = order;
        return this;
    }

    @Override
    public String toString() {
        return "Query{" +
                "pageSize=" + pageSize +
                ", pageNum=" + pageNum +
                ", order='" + order + '\'' +
                '}';
    }
}
