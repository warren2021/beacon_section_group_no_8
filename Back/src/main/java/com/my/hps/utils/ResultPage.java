package com.my.hps.utils;

import java.util.Map;

public class ResultPage<T> extends  Result{

    private Integer pageSize;

    private Integer pageNum;

    private Integer total;

    private Map<String,Object> data;

    public Integer getPageSize() {
        return pageSize;
    }

    public ResultPage<T> setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public ResultPage<T> setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
        return this;
    }

    public Integer getTotal() {
        return total;
    }

    public ResultPage<T> setTotal(Integer total) {
        this.total = total;
        return this;
    }

    @Override
    public Map<String, Object> getData() {
        return data;
    }

    public ResultPage<T> setData(Map<String, Object> data) {
        this.data = data;
        return this;
    }
}
