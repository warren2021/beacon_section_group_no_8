package com.my.hps.mapper;

import com.my.hps.model.Customer;
import com.my.hps.utils.Query;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CustomerMapper {
    Customer selectByEmail(Customer customer);

    void register(Customer customer);

    void update(Customer customer);

    Customer selectOne(Long customerId);

    List<Customer> getList(@Param("param") Customer customer, @Param("query") Query query);

    Integer queryListCount(Customer customer);
}
