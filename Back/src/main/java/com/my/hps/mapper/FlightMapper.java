package com.my.hps.mapper;

import com.my.hps.model.FlightEntity;
import com.my.hps.utils.Query;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FlightMapper {
    void update(FlightEntity flightEntity);

    void delete(Long flightId);

    void add(FlightEntity flightEntity);

    List<FlightEntity> queryList(@Param("flight") FlightEntity flightEntity, @Param("query") Query query);

    FlightEntity selectOne(Long flightId);

    List<FlightEntity> defaultList();

    Integer queryListCount(FlightEntity flightEntity);

    Integer defaultListCount();

    List<FlightEntity> getDomestic();

    List<FlightEntity> getNotDomestic();

    List<FlightEntity> queryAll(@Param("flight") FlightEntity flightEntity, @Param("query") Query query);

    Integer queryAllCount(FlightEntity flightEntity);
}
