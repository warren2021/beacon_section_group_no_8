package com.my.hps.mapper;

import com.my.hps.model.CertificateEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CertificateMapper {
    List<CertificateEntity> query(Long customerId);

    void update(CertificateEntity certificateEntity);

    void delete(Long certificateId);

    void add(CertificateEntity creditCard);

    List<CertificateEntity> queryByIds(@Param("arr") Object[] arr);
}
