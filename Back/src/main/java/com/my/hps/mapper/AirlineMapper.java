package com.my.hps.mapper;

import com.my.hps.model.AirlineEntity;
import com.my.hps.utils.Query;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AirlineMapper {
    void add(AirlineEntity airlineEntity);

    void delete(Long airId);

    void update(AirlineEntity airlineEntity);

    List<AirlineEntity> query(@Param("param")AirlineEntity param,@Param("query") Query query);

    Integer queryListCount(AirlineEntity param);
}
