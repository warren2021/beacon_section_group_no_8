package com.my.hps.mapper;

import com.my.hps.model.AirportEntity;
import com.my.hps.utils.Query;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AirportMapper {
    List<AirportEntity> query(@Param("param") AirportEntity airportEntity, @Param("query") Query query);

    void add(AirportEntity airportEntity);

    void delete(Long airportId);

    void update(AirportEntity airportEntity);

    Integer queryListCount(AirportEntity airportEntity);
}
