package com.my.hps.mapper;

import com.my.hps.model.BookingEntity;
import com.my.hps.utils.Query;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BookingMapper {
    void create(BookingEntity bookingEntity);

    List<BookingEntity> query(@Param("customerId") Long customerId, @Param("status") Integer status, @Param("query") Query query);

    BookingEntity selectOne(Long bookId);

    void update(BookingEntity setState);

    Long selectByCust(Long customerId);

    Integer queryCount(@Param("customerId") Long customerId, @Param("status") Integer status);
}
