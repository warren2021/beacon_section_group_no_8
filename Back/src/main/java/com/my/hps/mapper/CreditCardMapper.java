package com.my.hps.mapper;

import com.my.hps.model.CreditCard;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CreditCardMapper {
    Long add(CreditCard creditCard);

    void update(CreditCard creditCard);

    void delete(Long creditCardId);

    List<CreditCard> queryByCustomer(Long customerId);

    CreditCard getById(Long creditCardId);
}
