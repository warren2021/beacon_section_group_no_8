package com.my.hps.mapper;

import com.my.hps.model.MileageEntity;
import com.my.hps.utils.Query;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MileageMapper {
    void add(MileageEntity param);

    Integer queryListCount(MileageEntity param);

    List<MileageEntity> getList(@Param("param") MileageEntity param, @Param("query") Query query);

    void update(MileageEntity param);

    void delete(Long mileageId);

    MileageEntity getById(Long mileageId);

    MileageEntity selectOne(Long fightId);
}
