package com.my.hps.mapper;

import com.my.hps.model.AddressEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface AddressMapper {
    void add(AddressEntity addressEntity);

    void update(AddressEntity addressEntity);

    List<AddressEntity> select(Long customerId);

    AddressEntity selectOne(Long addressID);

    void delete(Long creditCardId);
}
