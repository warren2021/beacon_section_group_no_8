package com.my.hps.controller;


import com.my.hps.service.CustomerService;
import com.my.hps.model.Customer;
import com.my.hps.utils.Md5Util;
import com.my.hps.utils.Query;
import com.my.hps.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/customer")
@Api(tags = "customer")
public class CustomerController {

    @Autowired
    private CustomerService service;

    @PostMapping("/register")
    public Result register(@ApiParam("email") @RequestParam String email,
                           @ApiParam("name") @RequestParam String name,
                           @ApiParam("password") @RequestParam String password) {
        Customer customer = new Customer().setEmail(email).setPassword(Md5Util.getMD5Str(password)).setCustomerName(name);
        return service.register(customer);

    }

    @PostMapping("/login")
    public Result login(@ApiParam("email") @RequestParam String email,
                        @ApiParam("password") @RequestParam String password,
                        @ApiParam("identity") @RequestParam Integer identity) {
        Customer customer = new Customer().setEmail(email).setPassword(Md5Util.getMD5Str(password)).setIdentity(identity);
        return service.login(customer);
    }

    @PostMapping("/update")
    public Result update(@RequestBody Customer customer) {
        return service.update(customer);
    }

    @PostMapping("/updatePwd")
    public Result updatePwd(@ApiParam("email") @RequestParam String email,
                            @ApiParam("pwd") @RequestParam String pwd,
                            @ApiParam("type") @RequestParam Integer type,
                            @ApiParam("newPwd") @RequestParam String newPwd) {
        return service.updatePwd(pwd, type, newPwd, email);
    }

    @GetMapping("/getList")
    @ApiOperation(value = "getList", response = Customer.class)
    public Object getList(@ApiParam("name") @RequestParam(required = false) String name,
                          @ApiParam("sex") @RequestParam(required = false) Integer sex,
                          @ApiParam("pageSize") @RequestParam(required = false, defaultValue = "10") Integer pageSize,
                          @ApiParam("pageNum") @RequestParam(required = false, defaultValue = "1") Integer pageNum) {
        Customer customer = new Customer().setCustomerName(name).setSex(sex);
        Query query = new Query(pageSize, pageNum, null);
        Map<String, Object> map = service.getList(customer,query);
        return new Result<>().setData(map);
    }

    @PostMapping("/check")
    public Result check(@ApiParam("email") @RequestParam String email) {
        return service.check(email);
    }

}
