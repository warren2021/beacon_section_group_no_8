package com.my.hps.controller;


import com.my.hps.service.AirportService;
import com.my.hps.model.AirportEntity;
import com.my.hps.utils.Query;
import com.my.hps.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/airport")
@Api(tags = "airport")
public class AirportController {


    @Autowired
    private AirportService service;

    @PostMapping("/add")
    public Result add(@RequestBody AirportEntity airportEntity) {

        return service.add(airportEntity);
    }

    @PostMapping("/delete")
    public Result delete(@ApiParam("airportId") @RequestParam Long airportId) {

        return service.delete(airportId);
    }

    @PostMapping("/update")
    public Result update(@RequestBody AirportEntity airportEntity) {

        return service.update(airportEntity);
    }

    @GetMapping("/query")
    public Result query(@ApiParam("airportName") @RequestParam(required = false) String airportName,
                        @ApiParam("IATA") @RequestParam(required = false) String IATA,
                        @ApiParam("airportTelephone") @RequestParam(required = false) String airportTelephone,
                        @ApiParam("pageSize") @RequestParam(required = false, defaultValue = "10") Integer pageSize,
                        @ApiParam("pageNum") @RequestParam(required = false, defaultValue = "1") Integer pageNum) {
        AirportEntity airportEntity = new AirportEntity().setAirportName(airportName).setIATA(IATA).setAirportTelephone(airportTelephone);
        Query query = new Query().setPageNum(pageNum).setPageSize(pageSize);
        return service.query(airportEntity,query);
    }
}
