package com.my.hps.controller;


import com.my.hps.service.FlightService;
import com.my.hps.model.FlightEntity;
import com.my.hps.utils.Query;
import com.my.hps.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping("/flight")
@Api(tags = "flight")
public class FlightController {

    @Autowired
    private FlightService service;


    @PostMapping("/add")
    public Result add(@RequestBody FlightEntity flightEntity) {
        return service.add(flightEntity);
    }

    @PostMapping("/delete")
    public Result delete(@ApiParam("flightId") @RequestParam Long flightId) {
        return service.delete(flightId);
    }

    @PostMapping("/update")
    public Result update(@RequestBody FlightEntity flightEntity) {
        return service.update(flightEntity);
    }


    @GetMapping("/queryList")
    @ApiOperation(value = "queryList", response = FlightEntity.class)
    public Result queryList(@ApiParam("airlineName") @RequestParam(required = false) String airlineName,
                            @ApiParam("startAddress") @RequestParam(required = false) String startAddress,
                            @ApiParam("destination") @RequestParam(required = false) String destination,
                            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                            @ApiParam("startDate") @RequestParam(required = false) Date startDate,
                            @ApiParam("flightNumber") @RequestParam(required = false) String flightNumber,
                            @ApiParam("departureCity") @RequestParam(required = false) String departureCity,
                            @ApiParam("destinationCity") @RequestParam(required = false) String destinationCity,
                            @ApiParam("seat") @RequestParam(required = false) Integer seat,
                            @ApiParam("pageSize") @RequestParam(required = false, defaultValue = "10") Integer pageSize,
                            @ApiParam("pageNum") @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                            @ApiParam("order f.start_date desc ") @RequestParam(required = false) String order) {
        FlightEntity flightEntity = new FlightEntity().
                setStartAddress(startAddress).
                setDestination(destination).
                setStartDate(startDate).
                setAirlineName(airlineName).
                setFlightNumber(flightNumber).
                setDestinationCity(destinationCity).
                setDepartureCity(departureCity).
                setSeat(seat);
        Query query = new Query(pageSize, pageNum, order);
        return service.queryList(flightEntity, query);
    }

    @GetMapping("/queryAll")
    @ApiOperation(value = "queryAll", response = FlightEntity.class)
    public Result queryAll(@ApiParam("airlineName") @RequestParam(required = false) String airlineName,
                           @ApiParam("startAddress") @RequestParam(required = false) String startAddress,
                           @ApiParam("destination") @RequestParam(required = false) String destination,
                           @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                           @ApiParam("startDate") @RequestParam(required = false) Date startDate,
                           @ApiParam("flightNumber") @RequestParam(required = false) String flightNumber,
                           @ApiParam("departureCity") @RequestParam(required = false) String departureCity,
                           @ApiParam("destinationCity") @RequestParam(required = false) String destinationCity,
                           @ApiParam("pageSize") @RequestParam(required = false, defaultValue = "10") Integer pageSize,
                           @ApiParam("pageNum") @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                           @ApiParam("order f.start_date desc ") @RequestParam(required = false) String order) {
        FlightEntity flightEntity = new FlightEntity().
                setStartAddress(startAddress).
                setDestination(destination).
                setStartDate(startDate).
                setAirlineName(airlineName).
                setFlightNumber(flightNumber).
                setDestinationCity(destinationCity).
                setDepartureCity(departureCity);
        Query query = new Query(pageSize, pageNum, order);
        return service.queryAll(flightEntity, query);
    }

    @GetMapping("/defaultList")
    public Result defaultList() {
        return service.defaultList();
    }

    @GetMapping("/selectOne")
    @ApiOperation(value = "selectOne", response = FlightEntity.class)
    public Result selectOne(@ApiParam("flightId") @RequestParam Long flightId) {

        return new Result().setCode(200).setData(service.selectOne(flightId));
    }

    @GetMapping("/getHomeList")
    @ApiOperation(value = "getHomeList", response = FlightEntity.class)
    public Object getHomeList() {
        return service.getHomeList();
    }

}
