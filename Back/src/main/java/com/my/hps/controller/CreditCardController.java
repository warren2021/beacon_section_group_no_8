package com.my.hps.controller;


import com.my.hps.service.CreditCardService;
import com.my.hps.model.CreditCard;
import com.my.hps.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("creditCard")
@Api(tags = "creditCard")
public class CreditCardController {

    @Autowired
    private CreditCardService service;

    @PostMapping("/add")
    public Result add(@RequestBody CreditCard creditCard,
                      @ApiParam("addressId") @RequestParam Long addressId) {
        return service.add(creditCard,addressId);
    }

    @PostMapping("/delete")
    public Result delete(@ApiParam("creditCardId") @RequestParam Long creditCardId) {
        return service.delete(creditCardId);
    }


    @PostMapping("/update")
    public Result update(@RequestBody CreditCard creditCard,
                         @ApiParam("addressId") @RequestParam Long addressId) {
        if(creditCard == null || creditCard.getCreditCardId() == null){
            return new Result().setCode(1000).setMessage("Credit card ID cannot be empty");
        }
        return service.update(creditCard,addressId);
    }

    @GetMapping("/queryByCustomer")
    public Result queryByCustomer(@ApiParam("customerId") @RequestParam Long customerId) {
        return service.queryByCustomer(customerId);
    }

}
