package com.my.hps.controller;


import com.my.hps.service.BookingService;
import com.my.hps.model.BookingEntity;
import com.my.hps.utils.Query;
import com.my.hps.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/booking")
@Api(tags = "booking")
public class BookingController {

    @Autowired
    private BookingService service;

    @PostMapping("/create")
    @ApiOperation(value = "create")
    public Result create(@RequestBody BookingEntity bookingEntity) {
        return service.create(bookingEntity.getFlightId(), bookingEntity.getCustomerId(), bookingEntity.getCertificateIds(), bookingEntity.getSeat(), bookingEntity.getCreditCardId());
    }

    @GetMapping("/query")
    @ApiOperation(value = "queryByCustomer")
    public Result query(@ApiParam("customerId") @RequestParam(required = false) Long customerId,
                        @ApiParam("status") @RequestParam(required = false) Integer status,
                        @ApiParam("pageSize") @RequestParam(required = false, defaultValue = "10") Integer pageSize,
                        @ApiParam("pageNum") @RequestParam(required = false, defaultValue = "1") Integer pageNum) {
        Query query = new Query(pageSize, pageNum, "");
        return service.query(customerId, status, query);

    }

    @GetMapping("/refund")
    @ApiOperation(value = "refund")
    public Result refund(@ApiParam("bookId") @RequestParam Long bookId) {

        return service.refund(bookId);

    }

    @GetMapping("/queryById")
    @ApiOperation(value = "queryById")
    public Result queryById(@ApiParam("bookingId") @RequestParam Long bookingId) {

        return service.queryById(bookingId);
    }


}
