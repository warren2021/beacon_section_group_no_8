package com.my.hps.controller;


import com.my.hps.service.CertificateService;
import com.my.hps.model.CertificateEntity;
import com.my.hps.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/certificate")
@Api(tags = "certificate")
public class CertificateController {

    @Autowired
    private CertificateService service;
//customer_id  passenger_name  passengers_telephone  certificate_num
    @PostMapping("/add")
    public Result add(@ApiParam("customerId") @RequestParam Long customerId,
                      @ApiParam("passengerName") @RequestParam String passengerName,
                      @ApiParam("passengersTelephone") @RequestParam String passengersTelephone,
                      @ApiParam("certificateNum") @RequestParam String certificateNum) {
        CertificateEntity creditCard=new CertificateEntity().setCustomerId(customerId).setCertificateNum(certificateNum).setPassengerName(passengerName).setPassengersTelephone(passengersTelephone);

        return service.add(creditCard);
    }

    @PostMapping("/delete")
    public Result delete( @ApiParam("ID") @RequestParam Long certificateId) {


        return service.delete(certificateId);
    }

    @PostMapping("/update")
    public Result update(@RequestBody CertificateEntity certificateEntity){

        return service.update(certificateEntity);
    }

    @GetMapping("/query")
    public Result query(@ApiParam("customerId") @RequestParam Long customerId) {


        return service.query(customerId);
    }
}
