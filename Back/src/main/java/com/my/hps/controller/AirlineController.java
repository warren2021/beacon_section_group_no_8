package com.my.hps.controller;

import com.my.hps.service.AirlineService;
import com.my.hps.model.AirlineEntity;
import com.my.hps.utils.Query;
import com.my.hps.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/airline")
@Api(tags = "airline")
public class AirlineController {

    @Autowired
    private AirlineService service;

    @PostMapping("/add")
    public Result add(@RequestBody AirlineEntity airlineEntity) {

        return service.add(airlineEntity);
    }

    @PostMapping("/delete")
    public Result delete(@ApiParam("airId") @RequestParam Long airId) {

        return service.delete(airId);
    }

    @PostMapping("/update")
    public Result update(@RequestBody AirlineEntity airlineEntity) {

        return service.update(airlineEntity);
    }

    @GetMapping("/query")
    public Result query(@ApiParam("airlineName") @RequestParam(required = false) String airlineName,
                        @ApiParam("airlineCode") @RequestParam(required = false) String airlineCode,
                        @ApiParam("pageSize") @RequestParam(required = false, defaultValue = "10") Integer pageSize,
                        @ApiParam("pageNum") @RequestParam(required = false, defaultValue = "1") Integer pageNum) {
        AirlineEntity param = new AirlineEntity().setAirlineName(airlineName).setAirlineCode(airlineCode);
        Query query = new Query().setPageSize(pageSize).setPageNum(pageNum);
        return service.query(param,query);
    }
}
