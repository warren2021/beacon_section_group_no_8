package com.my.hps.controller;

import com.my.hps.model.MileageEntity;
import com.my.hps.service.MileageService;
import com.my.hps.utils.Query;
import com.my.hps.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/mileage")
@Api(tags = "mileage")
public class MileageController {

    @Autowired
    private MileageService service;

    @PostMapping("/add")
    @ApiOperation("add")
    public Object add(@RequestBody MileageEntity param) {
        if(param == null || param.getFlightId() == null){
            return new Result<>().setCode(10000).setMessage("Wrong parameter");
        }
        MileageEntity result = service.selectOne(param.getFlightId());
        if(result != null){
            return new Result<>().setCode(10000).setMessage("Repeat add");
        }
        service.add(param);
        return new Result<>();
    }

    @GetMapping("/getList")
    @ApiOperation(value = "getList", response = MileageEntity.class)
    public Object getList(@ApiParam("flightId") @RequestParam(required = false) Long flightId,
                          @ApiParam("pageSize") @RequestParam(required = false, defaultValue = "10") Integer pageSize,
                          @ApiParam("pageNum") @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                          @ApiParam("order f.start_date desc ") @RequestParam(required = false) String order) {
        MileageEntity param = new MileageEntity().setFlightId(flightId);
        Query query = new Query(pageSize, pageNum, order);
        Map<String, Object> map = service.getList(param, query);
        return new Result<>().setData(map);
    }

    @PostMapping("/update")
    @ApiOperation("update")
    public Object update(@RequestBody MileageEntity param) {
        if(param == null || param.getMileageId() == null){
            return new Result<>().setCode(10000);
        }
        service.update(param);
        return new Result<>();
    }

    @PostMapping("/delete")
    @ApiOperation("delete")
    public Object delete(@ApiParam(value = "id", required = true) @RequestParam Long mileageId) {
        service.delete(mileageId);
        return new Result<>();
    }

    @GetMapping("/getById")
    @ApiOperation(value = "getById", response = MileageEntity.class)
    public Object getById(@ApiParam(value = "id", required = true) @RequestParam Long mileageId){
        MileageEntity result = service.getById(mileageId);
        return new Result<>().setData(result);
    }

}
