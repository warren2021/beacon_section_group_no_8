package com.my.hps.controller;

import com.my.hps.service.AddressService;
import com.my.hps.model.AddressEntity;

import com.my.hps.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/address")
@Api(tags = "address")
public class AddressController {

    @Autowired
    private AddressService service;

    @PostMapping("/add")
    public Result register(@RequestBody AddressEntity addressEntity) {

        return service.add(addressEntity);
    }
    @PostMapping("/delete")
    public Result delete(@ApiParam("addressId") @RequestParam Long addressId) {

        return service.delete(new AddressEntity().setAddressId(addressId));
    }
    @PostMapping("/update")
    public Result update(@RequestBody AddressEntity addressEntity) {

        return service.update(addressEntity);
    }
    @GetMapping("/select")
    public Result select(@ApiParam("customerId") @RequestParam Long customerId) {

        return service.select(new AddressEntity().setCustomerId(customerId));
    }

}