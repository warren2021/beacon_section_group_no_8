package com.my.hps.filter;

import com.my.hps.utils.RequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

@WebFilter
public class MyFilter implements Filter {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest hrequest = (HttpServletRequest)servletRequest;
        RequestWrapper requestWrapper = new RequestWrapper(hrequest);
        String body = requestWrapper.getBody().replace("\n", "");
        Map<String, String[]> parameterMap = hrequest.getParameterMap();
        StringBuffer sb = new StringBuffer();

        parameterMap.forEach((k,v)->{
            sb.append("\t").append(k).append(":");
            for (int i = 0; i < v.length; i++) {
                sb.append(v[i]);
            }
        });
        log.info(hrequest.getRemoteHost() + "\t" +hrequest.getMethod() + "request："+hrequest.getRequestURI()+" Parameter：{"+sb.toString()+body+"}");

        filterChain.doFilter(requestWrapper, servletResponse);
    }
    @Override
    public void destroy() {
    }
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
}