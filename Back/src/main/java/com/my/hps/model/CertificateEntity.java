package com.my.hps.model;

import io.swagger.annotations.ApiModelProperty;

public class CertificateEntity {

    @ApiModelProperty(value = "")
    private Long certificateId;
    private Long customerId;
    private String passengerName;
    private String passengersTelephone;
    private String certificateNum;

    public Long getCertificateId() {
        return certificateId;
    }

    public CertificateEntity setCertificateId(Long certificateId) {
        this.certificateId = certificateId;
        return this;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public CertificateEntity setCustomerId(Long customerId) {
        this.customerId = customerId;
        return this;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public CertificateEntity setPassengerName(String passengerName) {
        this.passengerName = passengerName;
        return this;
    }

    public String getPassengersTelephone() {
        return passengersTelephone;
    }

    public CertificateEntity setPassengersTelephone(String passengersTelephone) {
        this.passengersTelephone = passengersTelephone;
        return this;
    }

    public String getCertificateNum() {
        return certificateNum;
    }

    public CertificateEntity setCertificateNum(String certificateNum) {
        this.certificateNum = certificateNum;
        return this;
    }
}
