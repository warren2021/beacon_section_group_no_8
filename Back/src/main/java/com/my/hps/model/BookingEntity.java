package com.my.hps.model;

import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import java.util.List;

public class BookingEntity {


    private Long bookingId;
    private String bookingNumber;
    private Long customerId;
    private Long flightId;
    private Integer seat;
    private Integer state;
    private String telephone;
    private String certificateIds;
    private Long departureAirport;
    private Date departureTime;
    private Long destinationAirport;
    private Date destinationTime;
    private Integer mileageSum;
    private Double price;
    private Long creditCardId;
    private Integer ticketSum;
    private Double priceSum;
    private List<CertificateEntity> certificateList;
    private Date createDate;
    private Date updateDate;
    private String destinationName;
    private String departureName;
    private String destinationCity;
    private String departureCity;
    private String flightNumber;
    private String airlineName;

    public String getAirlineName() {
        return airlineName;
    }

    public BookingEntity setAirlineName(String airlineName) {
        this.airlineName = airlineName;
        return this;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public BookingEntity setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
        return this;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public BookingEntity setDestinationName(String destinationName) {
        this.destinationName = destinationName;
        return this;
    }

    public String getDepartureName() {
        return departureName;
    }

    public BookingEntity setDepartureName(String departureName) {
        this.departureName = departureName;
        return this;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public BookingEntity setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
        return this;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public BookingEntity setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
        return this;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public BookingEntity setCreateDate(Date createDate) {
        this.createDate = createDate;
        return this;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public BookingEntity setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public Long getBookingId() {
        return bookingId;
    }

    public BookingEntity setBookingId(Long bookingId) {
        this.bookingId = bookingId;
        return this;
    }

    public String getBookingNumber() {
        return bookingNumber;
    }

    public BookingEntity setBookingNumber(String bookingNumber) {
        this.bookingNumber = bookingNumber;
        return this;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public BookingEntity setCustomerId(Long customerId) {
        this.customerId = customerId;
        return this;
    }

    public Long getFlightId() {
        return flightId;
    }

    public BookingEntity setFlightId(Long flightId) {
        this.flightId = flightId;
        return this;
    }

    public Integer getSeat() {
        return seat;
    }

    public BookingEntity setSeat(Integer seat) {
        this.seat = seat;
        return this;
    }

    public Integer getState() {
        return state;
    }

    public BookingEntity setState(Integer state) {
        this.state = state;
        return this;
    }

    public String getTelephone() {
        return telephone;
    }

    public BookingEntity setTelephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public String getCertificateIds() {
        return certificateIds;
    }

    public BookingEntity setCertificateIds(String certificateIds) {
        this.certificateIds = certificateIds;
        return this;
    }

    public Long getDepartureAirport() {
        return departureAirport;
    }

    public BookingEntity setDepartureAirport(Long departureAirport) {
        this.departureAirport = departureAirport;
        return this;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public BookingEntity setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
        return this;
    }

    public Long getDestinationAirport() {
        return destinationAirport;
    }

    public BookingEntity setDestinationAirport(Long destinationAirport) {
        this.destinationAirport = destinationAirport;
        return this;
    }

    public Date getDestinationTime() {
        return destinationTime;
    }

    public BookingEntity setDestinationTime(Date destinationTime) {
        this.destinationTime = destinationTime;
        return this;
    }

    public Integer getMileageSum() {
        return mileageSum;
    }

    public BookingEntity setMileageSum(Integer mileageSum) {
        this.mileageSum = mileageSum;
        return this;
    }

    public Double getPrice() {
        return price;
    }

    public BookingEntity setPrice(Double price) {
        this.price = price;
        return this;
    }

    public Long getCreditCardId() {
        return creditCardId;
    }

    public BookingEntity setCreditCardId(Long creditCardId) {
        this.creditCardId = creditCardId;
        return this;
    }

    public Integer getTicketSum() {
        return ticketSum;
    }

    public BookingEntity setTicketSum(Integer ticketSum) {
        this.ticketSum = ticketSum;
        return this;
    }

    public Double getPriceSum() {
        return priceSum;
    }

    public BookingEntity setPriceSum(Double priceSum) {
        this.priceSum = priceSum;
        return this;
    }

    public List<CertificateEntity> getCertificateList() {
        return certificateList;
    }

    public BookingEntity setCertificateList(List<CertificateEntity> certificateList) {
        this.certificateList = certificateList;
        return this;
    }
}
