package com.my.hps.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class FlightEntity {


    @ApiModelProperty(value = "id")
    private Long flightId;
    private Long airlineId;
    private String flightNumber;
    private Date flightDate;
    private String startAddress;
    private String destination;
    private Date startDate;
    private Date arriveDate;
    private Integer firstClassNum;
    private Integer economyClassNum;
    private Double firstClassPrice;
    private Double economyClassPrice;
    private Long destinationAirport;
    private Integer mileageSum;
    private Long departureAirport;
    private String airlineName;
    private String destinationName;
    private String departureName;
    private String destinationCity;
    private String departureCity;
    private Integer economyMileage;
    private Integer firstMileage;
    private Long mileageId;
    private Integer seat;

    public Integer getSeat() {
        return seat;
    }

    public FlightEntity setSeat(Integer seat) {
        this.seat = seat;
        return this;
    }

    public Integer getEconomyMileage() {
        return economyMileage;
    }

    public FlightEntity setEconomyMileage(Integer economyMileage) {
        this.economyMileage = economyMileage;
        return this;
    }

    public Integer getFirstMileage() {
        return firstMileage;
    }

    public FlightEntity setFirstMileage(Integer firstMileage) {
        this.firstMileage = firstMileage;
        return this;
    }

    public Long getMileageId() {
        return mileageId;
    }

    public FlightEntity setMileageId(Long mileageId) {
        this.mileageId = mileageId;
        return this;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public FlightEntity setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
        return this;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public FlightEntity setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
        return this;
    }

    public Long getFlightId() {
        return flightId;
    }

    public FlightEntity setFlightId(Long flightId) {
        this.flightId = flightId;
        return this;
    }

    public Long getAirlineId() {
        return airlineId;
    }

    public FlightEntity setAirlineId(Long airlineId) {
        this.airlineId = airlineId;
        return this;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public FlightEntity setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
        return this;
    }

    public Date getFlightDate() {
        return flightDate;
    }

    public FlightEntity setFlightDate(Date flightDate) {
        this.flightDate = flightDate;
        return this;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public FlightEntity setStartAddress(String startAddress) {
        this.startAddress = startAddress;
        return this;
    }

    public String getDestination() {
        return destination;
    }

    public FlightEntity setDestination(String destination) {
        this.destination = destination;
        return this;
    }

    public Date getStartDate() {
        return startDate;
    }

    public FlightEntity setStartDate(Date startDate) {
        this.startDate = startDate;
        return this;
    }

    public Date getArriveDate() {
        return arriveDate;
    }

    public FlightEntity setArriveDate(Date arriveDate) {
        this.arriveDate = arriveDate;
        return this;
    }

    public Integer getFirstClassNum() {
        return firstClassNum;
    }

    public FlightEntity setFirstClassNum(Integer firstClassNum) {
        this.firstClassNum = firstClassNum;
        return this;
    }

    public Integer getEconomyClassNum() {
        return economyClassNum;
    }

    public FlightEntity setEconomyClassNum(Integer economyClassNum) {
        this.economyClassNum = economyClassNum;
        return this;
    }

    public Double getFirstClassPrice() {
        return firstClassPrice;
    }

    public FlightEntity setFirstClassPrice(Double firstClassPrice) {
        this.firstClassPrice = firstClassPrice;
        return this;
    }

    public Double getEconomyClassPrice() {
        return economyClassPrice;
    }

    public FlightEntity setEconomyClassPrice(Double economyClassPrice) {
        this.economyClassPrice = economyClassPrice;
        return this;
    }

    public Long getDestinationAirport() {
        return destinationAirport;
    }

    public FlightEntity setDestinationAirport(Long destinationAirport) {
        this.destinationAirport = destinationAirport;
        return this;
    }

    public Integer getMileageSum() {
        return mileageSum;
    }

    public FlightEntity setMileageSum(Integer mileageSum) {
        this.mileageSum = mileageSum;
        return this;
    }

    public Long getDepartureAirport() {
        return departureAirport;
    }

    public FlightEntity setDepartureAirport(Long departureAirport) {
        this.departureAirport = departureAirport;
        return this;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public FlightEntity setAirlineName(String airlineName) {
        this.airlineName = airlineName;
        return this;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public FlightEntity setDestinationName(String destinationName) {
        this.destinationName = destinationName;
        return this;
    }

    public String getDepartureName() {
        return departureName;
    }

    public FlightEntity setDepartureName(String departureName) {
        this.departureName = departureName;
        return this;
    }

    @Override
    public String toString() {
        return "FlightEntity{" +
                "flightId=" + flightId +
                ", airlineId=" + airlineId +
                ", flightNumber='" + flightNumber + '\'' +
                ", flightDate=" + flightDate +
                ", startAddress='" + startAddress + '\'' +
                ", destination='" + destination + '\'' +
                ", startDate=" + startDate +
                ", arriveDate=" + arriveDate +
                ", firstClassNum=" + firstClassNum +
                ", economyClassNum=" + economyClassNum +
                ", firstClassPrice=" + firstClassPrice +
                ", economyClassPrice=" + economyClassPrice +
                ", destinationAirport=" + destinationAirport +
                ", mileageSum=" + mileageSum +
                ", departureAirport=" + departureAirport +
                ", airlineName='" + airlineName + '\'' +
                ", destinationName='" + destinationName + '\'' +
                ", departureName='" + departureName + '\'' +
                ", destinationCity='" + destinationCity + '\'' +
                ", departureCity='" + departureCity + '\'' +
                '}';
    }
}
