package com.my.hps.model;

import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

public class CreditCard {

    @ApiModelProperty(value = "id")
    private Long creditCardId;
    private Long customerId;
    private String cardNumber;
    private String bank;
    private String name;
    private Date expiration;
    private String cvcCode;
    private String addrDetail;
    private String addrid;

    public String getAddrDetail() {
        return addrDetail;
    }

    public CreditCard setAddrDetail(String addrDetail) {
        this.addrDetail = addrDetail;
        return this;
    }

    public String getAddrid() {
        return addrid;
    }

    public CreditCard setAddrid(String addrid) {
        this.addrid = addrid;
        return this;
    }

    public Long getCreditCardId() {
        return creditCardId;
    }

    public CreditCard setCreditCardId(Long creditCardId) {
        this.creditCardId = creditCardId;
        return this;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public CreditCard setCustomerId(Long customerId) {
        this.customerId = customerId;
        return this;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public CreditCard setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
        return this;
    }

    public String getBank() {
        return bank;
    }

    public CreditCard setBank(String bank) {
        this.bank = bank;
        return this;
    }

    public String getName() {
        return name;
    }

    public CreditCard setName(String name) {
        this.name = name;
        return this;
    }

    public Date getExpiration() {
        return expiration;
    }

    public CreditCard setExpiration(Date expiration) {
        this.expiration = expiration;
        return this;
    }

    public String getCvcCode() {
        return cvcCode;
    }

    public CreditCard setCvcCode(String cvcCode) {
        this.cvcCode = cvcCode;
        return this;
    }

    @Override
    public String toString() {
        return "CreditCard{" +
                "creditCardId=" + creditCardId +
                ", customerId=" + customerId +
                ", cardNumber='" + cardNumber + '\'' +
                ", bank='" + bank + '\'' +
                ", name='" + name + '\'' +
                ", expiration=" + expiration +
                ", cvcCode='" + cvcCode + '\'' +
                '}';
    }
}
