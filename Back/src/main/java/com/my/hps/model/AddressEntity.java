package com.my.hps.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Address")
public class AddressEntity {
    @ApiModelProperty(value = "id")
    private Long addressId;
    private Long creditCardId;
    private String addrCountry;
    private String addrState;
    private Long customerId;
    private String addrCity;
    private String addrArea;
    private String addrDetail;
    private String postalCode;

    public String getPostalCode() {
        return postalCode;
    }

    public AddressEntity setPostalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public AddressEntity setCustomerId(Long customerId) {
        this.customerId = customerId;
        return this;
    }

    public Long getAddressId() {
        return addressId;
    }

    public AddressEntity setAddressId(Long addressId) {
        this.addressId = addressId;
        return this;
    }

    public Long getCreditCardId() {
        return creditCardId;
    }

    public AddressEntity setCreditCardId(Long creditCardId) {
        this.creditCardId = creditCardId;
        return this;
    }

    public String getAddrCountry() {
        return addrCountry;
    }

    public AddressEntity setAddrCountry(String addrCountry) {
        this.addrCountry = addrCountry;
        return this;
    }

    public String getAddrState() {
        return addrState;
    }

    public AddressEntity setAddrState(String addrState) {
        this.addrState = addrState;
        return this;
    }

    public String getAddrCity() {
        return addrCity;
    }

    public AddressEntity setAddrCity(String addrCity) {
        this.addrCity = addrCity;
        return this;
    }

    public String getAddrArea() {
        return addrArea;
    }

    public AddressEntity setAddrArea(String addrArea) {
        this.addrArea = addrArea;
        return this;
    }

    public String getAddrDetail() {
        return addrDetail;
    }

    public AddressEntity setAddrDetail(String addrDetail) {
        this.addrDetail = addrDetail;
        return this;
    }
}
