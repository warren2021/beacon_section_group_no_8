package com.my.hps.model;

import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

public class MileageEntity {

    @ApiModelProperty(value = "id")
    private Long mileageId;
    private Integer firstMileage;
    private Integer economyMileage;
    private Long flightId;
    private Date createDate;
    private Date updateDate;

    public Long getMileageId() {
        return mileageId;
    }

    public MileageEntity setMileageId(Long mileageId) {
        this.mileageId = mileageId;
        return this;
    }

    public Integer getFirstMileage() {
        return firstMileage;
    }

    public MileageEntity setFirstMileage(Integer firstMileage) {
        this.firstMileage = firstMileage;
        return this;
    }

    public Integer getEconomyMileage() {
        return economyMileage;
    }

    public MileageEntity setEconomyMileage(Integer economyMileage) {
        this.economyMileage = economyMileage;
        return this;
    }

    public Long getFlightId() {
        return flightId;
    }

    public MileageEntity setFlightId(Long flightId) {
        this.flightId = flightId;
        return this;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public MileageEntity setCreateDate(Date createDate) {
        this.createDate = createDate;
        return this;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public MileageEntity setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
        return this;
    }
}
