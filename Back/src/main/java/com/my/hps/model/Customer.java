package com.my.hps.model;

import java.util.Date;

public class Customer {


   private Long customerId;

    private String customerName;

    private String email;

    private Integer sex;

    private String country;

    private String state;

    private String city;

    private Integer Identity;

    private String password;

    private String payPassword;

    private String portrait;


    private Long airportId;

    private Date createDate;

    private Long createBy;

    private Date updateDate;

    private Long updateBy;

    private Long countMileageSum;

    private Integer mileageNum;

    public Integer getMileageNum() {
        return mileageNum;
    }

    public Customer setMileageNum(Integer mileageNum) {
        this.mileageNum = mileageNum;
        return this;
    }

    public Long getCountMileageSum() {
        return countMileageSum;
    }

    public Customer setCountMileageSum(Long countMileageSum) {
        this.countMileageSum = countMileageSum;
        return this;
    }

    public String getPortrait() {
        return portrait;
    }

    public Customer setPortrait(String portrait) {
        this.portrait = portrait;
        return this;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public Customer setCustomerId(Long customerId) {
        this.customerId = customerId;
        return this;
    }

    public String getCustomerName() {
        return customerName;
    }

    public Customer setCustomerName(String customerName) {
        this.customerName = customerName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Customer setEmail(String email) {
        this.email = email;
        return this;
    }

    public Integer getSex() {
        return sex;
    }

    public Customer setSex(Integer sex) {
        this.sex = sex;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Customer setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getState() {
        return state;
    }

    public Customer setState(String state) {
        this.state = state;
        return this;
    }

    public String getCity() {
        return city;
    }

    public Customer setCity(String city) {
        this.city = city;
        return this;
    }

    public Integer getIdentity() {
        return Identity;
    }

    public Customer setIdentity(Integer identity) {
        Identity = identity;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Customer setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getPayPassword() {
        return payPassword;
    }

    public Customer setPayPassword(String payPassword) {
        this.payPassword = payPassword;
        return this;
    }

    public Long getAirportId() {
        return airportId;
    }

    public Customer setAirportId(Long airportId) {
        this.airportId = airportId;
        return this;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public Customer setCreateDate(Date createDate) {
        this.createDate = createDate;
        return this;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public Customer setCreateBy(Long createBy) {
        this.createBy = createBy;
        return this;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public Customer setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public Customer setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
        return this;
    }
}
