package com.my.hps.model;


import io.swagger.annotations.ApiModelProperty;

public class AirportEntity {


    @ApiModelProperty(value = "id")
    private Long airportId;
    private String IATA;
    private String airportName;
    private String country;
    private String state;
    private String city;
    private String addrDetail1;
    private String addrDetail2;
    private String airportTelephone;
    private Integer isDomestic;

    public Integer getIsDomestic() {
        return isDomestic;
    }

    public AirportEntity setIsDomestic(Integer isDomestic) {
        this.isDomestic = isDomestic;
        return this;
    }

    public Long getAirportId() {
        return airportId;
    }

    public AirportEntity setAirportId(Long airportId) {
        this.airportId = airportId;
        return this;
    }

    public String getIATA() {
        return IATA;
    }

    public AirportEntity setIATA(String IATA) {
        this.IATA = IATA;
        return this;
    }

    public String getAirportName() {
        return airportName;
    }

    public AirportEntity setAirportName(String airportName) {
        this.airportName = airportName;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public AirportEntity setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getState() {
        return state;
    }

    public AirportEntity setState(String state) {
        this.state = state;
        return this;
    }

    public String getCity() {
        return city;
    }

    public AirportEntity setCity(String city) {
        this.city = city;
        return this;
    }

    public String getAddrDetail1() {
        return addrDetail1;
    }

    public AirportEntity setAddrDetail1(String addrDetail1) {
        this.addrDetail1 = addrDetail1;
        return this;
    }

    public String getAddrDetail2() {
        return addrDetail2;
    }

    public AirportEntity setAddrDetail2(String addrDetail2) {
        this.addrDetail2 = addrDetail2;
        return this;
    }

    public String getAirportTelephone() {
        return airportTelephone;
    }

    public AirportEntity setAirportTelephone(String airportTelephone) {
        this.airportTelephone = airportTelephone;
        return this;
    }
}
