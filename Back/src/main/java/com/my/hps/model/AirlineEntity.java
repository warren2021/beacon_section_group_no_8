package com.my.hps.model;


public class AirlineEntity {

    private Long airlineId;

    private String airlineName;

    private String airlineCode;

    private String country;





    public Long getAirlineId() {
        return airlineId;
    }

    public AirlineEntity setAirlineId(Long airlineId) {
        this.airlineId = airlineId;
        return this;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public AirlineEntity setAirlineName(String airlineName) {
        this.airlineName = airlineName;
        return this;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public AirlineEntity setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public AirlineEntity setCountry(String country) {
        this.country = country;
        return this;
    }


}
