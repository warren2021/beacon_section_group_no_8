package com.my.hps.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Controller
@Configuration
public class LogConfig {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${user.home}")
    private String userName;

    @Value("${server.port}")
    private String port;

    @Bean
    public ApplicationRunner applicationRunner() {
        return applicationArguments -> {
            try {
                InetAddress ia = InetAddress.getLocalHost();
                log.info("Start Successful：" + "http://" + ia.getHostAddress() + ":" + port + "/");
                log.info("${user.home} ：" + userName);
            } catch (UnknownHostException ex) {
                ex.printStackTrace();
            }
        };
    }
}