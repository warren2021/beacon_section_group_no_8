CREATE TABLE customer(
    email VARCHAR(32) UNIQUE NOT NULL,
    password VARCHAR(64) NOT NULL,
    PRIMARY KEY (email)
);
CREATE TABLE address(
    addressID BIGINT NOT NULL,
    postalCode VARCHAR(8) NOT NULL,
    state VARCHAR(64) NOT NULL,
    city VARCHAR(64) NOT NULL,
    country VARCHAR(64) NOT NULL,
    detail1 VARCHAR(128) NOT NULL,
    detail2 VARCHAR(128) NOT NULL,
    PRIMARY KEY (addressID)
);
CREATE TABLE Customer_Address(
    addressID BIGINT NOT NULL,
	email VARCHAR(32) UNIQUE NOT NULL,
	PRIMARY KEY (addressID,email)
	FOREIGN KEY (addressID) REFERENCES address,
	FOREIGN KEY (email) REFERENCES customer
);
);
CREATE TABLE creditCard(
    creditCardID BIGINT NOT NULL,
   	CardName VARCHAR(64) NOt NULL,
    expiration VARCHAR(8) NOT NULL,
    cvcCode VARCHAR(8) NOT NULL,
    cardNumber VARCHAR(32) NOT NULL,
    PRIMARY KEY (creditCardID),
);

CREATE TABLE customerCreditCard(
    creditCardID BIGINT NOT NULL,
    email VARCHAR(32) UNIQUE NOT NULL,    
    PRIMARY KEY (creditCardID,email),
	FOREIGN KEY (email) REFERENCES customer,
	FOREIGN KEY (creditCardID) REFERENCES creditCard
);

CREATE TABLE CreditCardAddress(
    creditCardID BIGINT NOT NULL,
    addressID BIGINT NOT NULL,    
    PRIMARY KEY (creditCardID,addressID)
	FOREIGN KEY (creditCardID) REFERENCES creditCard,
	FOREIGN KEY (addressID) REFERENCES address
);

CREATE TABLE airPort(
    airPortID CHAR(3) NOT NULL,
    name VARCHAR(64) NOT NULL,
    country VARCHAR(64) NOT NULL,
    state VARCHAR(64) NOT NULL,
    PRIMARY KEY (airPortID)
);

CREATE TABLE HomeAirPort(
    airPortID CHAR(3) NOT NULL,
    email VARCHAR(32) UNIQUE NOT NULL,
    PRIMARY KEY (airPortID,email)
	FOREIGN KEY (airPortID) REFERENCES airPort,
	FOREIGN KEY (email) REFERENCES customer
	
);

CREATE TABLE airLine(
      airLineID CHAR(2) NOT NULL,
      name varchar(64) NOT NULL,
      country VARCHAR(64) NOT NULL,
      PRIMARY KEY (airLineID)
	  
);


CREATE TABLE flight(
     airLineID CHAR(2) NOT NULL,
     flightNumber INT NOT NULL,
     flightDate TIMESTAMP NOT NULL,
     flightLength DECIMAL(10,2) NOT NULL,
     departTime TIMESTAMP NOT NULL,
     arriveTime TIMESTAMP NOT NULL,
     arriveAirport CHAR(3) NOT NULL,
     departAirport CHAR(3) NOT NULL,
	 firstClassNum INT NOT NULL,
	 economyClassNum INT NOT NULL,
     PRIMARY KEY (airLineID,flightNumber,flightDate),
          
);

CREATE TABLE AirPort_Flight(
    airPortID CHAR(3) NOT NULL,
    airLineID CHAR(2) NOT NULL,
    flightNumber INT NOT NULL,
    flightDate TIMESTAMP NOT NULL,
    PRIMARY KEY (airPortID,airLineID,flightNumber,flightDate)
	FOREIGN KEY (airPortID) REFERENCES airPort,
	FOREIGN KEY (airLineID,flightNumber,flightDate) REFERENCES flight
);

CREATE TABLE  price(
     airLineID CHAR(2) NOT NULL,
     flightNumber INT NOT NULL,
     flightDate TIMESTAMP NOT NULL,
     priceDate TIMESTAMP NOT NULL,
     firstClassPrice  NUMERIC(6,2) NOT NULL,
     economyClassPrice NUMERIC(6,2) NOT NULL,
     PRIMARY KEY(airLineID, flightNumber,flightDate,priceDate, firstClassPrice,enconmyClassPrice),
     FOREIGN KEY (airLineID, flightNumber,flightDate,) REFERENCES flight 
);

CREATE TABLE booking(
     bookingID BIGINT NOT NULL,
     seatType VARCHAR(32) NOT NULL,
     PRIMARY KEY (bookingID),
    
);

CREATE TABLE booking_flight(
     bookingID BIGINT NOT NULL,
     airLineID CHAR(2) NOT NULL,
     flightNumber INT NOT NULL,
     flightDate TIMESTAMP NOT NULL,
     PRIMARY KEY (bookingID,airLineID,flightNumber,flightDate)
	 FOREIGN KEY (bookingID) REFERENCES booking,
	 FOREIGN KEY (airLineID,flightNumber,flightDate) REFERENCES flight
);

CREATE TABLE Customer_Booking(
     bookingID BIGINT NOT NULL,
	 creditCardID BIGINT NOT NULL,
	 email VARCHAR(32) UNIQUE NOT NULL,
	 PRIMARY KEY (bookingID,creditCardID,email),
     FOREIGN KEY (creditCardID) REFERENCES creditCard,
     FOREIGN KEY (email) REFERENCES customer,
     FOREIGN KEY (bookingID) REFERENCES Booking
);	


CREATE TABLE mileage(
    mileageID  BIGINT NOT NULL,
    mileageSum DECIMAL(10,2) NOT NULL,
    mileage DECIMAL(10,2) NOT NULL,
    mileageDate DATE NOT NULL,
    PRIMARY KEY(mileageID),
);

CREATE TABLE Customer_Mileage(
    mileageID  BIGINT NOT NULL,
    email VARCHAR(32) UNIQUE NOT NULL,
    PRIMARY KEY(mileageID,email),
	FOREIGN KEY (mileageID) REFERENCES mileage,
	FOREIGN KEY (email) REFERENCES customer
	
);

CREATE TABLE Bouns_Mileage(
    mileageID  BIGINT NOT NULL,
    airLineID CHAR(2) NOT NULL,
    PRIMARY KEY(mileageID,airLineID),
	FOREIGN KEY (mileageID) REFERENCES mileage,
	FOREIGN KEY (airLineID) REFERENCES Airline
);


