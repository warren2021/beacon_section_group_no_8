/*
SQLyog Professional v12.08 (64 bit)
MySQL - 8.0.21 : Database - hps
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`hps` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE hps;

/*Table structure for table address_ */

DROP TABLE IF EXISTS address_;

CREATE TABLE address_ (
  address_id bigint NOT NULL AUTO_INCREMENT,
  credit_card_id bigint DEFAULT NULL COMMENT '信用卡ID',
  customer_id bigint NOT NULL COMMENT '用户ID',
  addr_country varchar(64) DEFAULT NULL COMMENT '国家',
  addr_state varchar(64) DEFAULT NULL COMMENT '洲',
  addr_city varchar(64) DEFAULT NULL COMMENT '市区',
  addr_area varchar(64) DEFAULT NULL COMMENT '地区',
  addr_detail varchar(64) DEFAULT NULL COMMENT '地址',
  postal_code varchar(64) DEFAULT NULL COMMENT '邮政编码',
  PRIMARY KEY (address_id)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='客户地址表';

/*Table structure for table airline_ */

DROP TABLE IF EXISTS airline_;

CREATE TABLE airline_ (
  airline_id bigint NOT NULL AUTO_INCREMENT,
  airline_name varchar(64) NOT NULL COMMENT '名称',
  airline_code varchar(8) NOT NULL COMMENT '唯一代码',
  country_ varchar(64) NOT NULL COMMENT '所属国家',
  PRIMARY KEY (airline_id)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8 COMMENT='航空公司';

/*Table structure for table airport_ */

DROP TABLE IF EXISTS airport_;

CREATE TABLE airport_ (
  airport_id bigint NOT NULL AUTO_INCREMENT,
  IATA_ varchar(12) DEFAULT NULL COMMENT '机场代码',
  airport_name varchar(128) DEFAULT NULL COMMENT '名称',
  country_ varchar(64) DEFAULT NULL COMMENT '国家',
  state_ varchar(64) DEFAULT NULL COMMENT '省份',
  city_ varchar(64) DEFAULT NULL COMMENT '市区',
  addr_detail_1 varchar(256) DEFAULT NULL COMMENT '详细地址1',
  addr_detail_2 varchar(256) DEFAULT NULL COMMENT '详细地址2',
  airport_telephone varchar(64) DEFAULT NULL COMMENT '机场电话',
  is_domestic int DEFAULT '1' COMMENT '国内(1是2不是)',
  PRIMARY KEY (airport_id)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COMMENT='机场';

/*Table structure for table booking_ */

DROP TABLE IF EXISTS booking_;

CREATE TABLE booking_ (
  booking_id bigint NOT NULL AUTO_INCREMENT,
  booking_number varchar(128) NOT NULL COMMENT '订单号',
  customer_id bigint NOT NULL COMMENT '用户ID',
  flight_id bigint NOT NULL COMMENT '航班ID',
  seat int NOT NULL COMMENT '是否是头等舱 1.是 2.不是',
  state_ int DEFAULT '1' COMMENT '1.已完成 2.未完成 3.已取消',
  telephone varchar(64) DEFAULT NULL COMMENT '电话',
  certificate_id varchar(128) DEFAULT NULL COMMENT '乘坐人',
  credit_card_id bigint DEFAULT NULL COMMENT '行用卡ID',
  departure_airport bigint DEFAULT NULL COMMENT '出发机场',
  departure_time datetime DEFAULT NULL COMMENT '起飞时间',
  destination_airport bigint DEFAULT NULL COMMENT '到达机场',
  destination_time datetime DEFAULT NULL COMMENT '到达时间',
  mileage_sum int DEFAULT '0' COMMENT '里程',
  price decimal(10,2) DEFAULT NULL COMMENT '单价',
  ticket_sum int DEFAULT NULL COMMENT '购票数量',
  price_sum decimal(10,2) DEFAULT NULL COMMENT '总票价',
  create_date datetime DEFAULT NULL COMMENT '创建时间',
  update_date datetime DEFAULT NULL COMMENT '退票时间',
  PRIMARY KEY (booking_id)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='订单';

/*Table structure for table certificate */

DROP TABLE IF EXISTS certificate;

CREATE TABLE certificate (
  certificate_id bigint NOT NULL AUTO_INCREMENT,
  customer_id bigint NOT NULL COMMENT '所属用户',
  passenger_name varchar(64) NOT NULL COMMENT '乘客名',
  passengers_telephone varchar(64) NOT NULL COMMENT '乘客电话',
  certificate_num varchar(128) NOT NULL COMMENT '乘客身份证编号',
  PRIMARY KEY (certificate_id)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Table structure for table credit_card */

DROP TABLE IF EXISTS credit_card;

CREATE TABLE credit_card (
  credit_card_id bigint NOT NULL AUTO_INCREMENT,
  customer_id bigint NOT NULL COMMENT '所属人',
  name_ varchar(64) DEFAULT NULL COMMENT '持卡人名字',
  card_number varchar(128) DEFAULT NULL COMMENT '信用卡编号',
  bank_ varchar(64) DEFAULT NULL COMMENT '所属银行',
  expiration datetime DEFAULT NULL COMMENT '日期',
  cvccode varchar(20) DEFAULT NULL COMMENT '银行卡背面4位密码',
  PRIMARY KEY (credit_card_id)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='客户信用卡表';

/*Table structure for table customer */

DROP TABLE IF EXISTS customer;

CREATE TABLE customer (
  customer_id bigint NOT NULL AUTO_INCREMENT,
  customer_name varchar(64) DEFAULT NULL COMMENT '名字',
  email varchar(256) NOT NULL COMMENT '邮箱',
  sex int DEFAULT '1' COMMENT '性别1男2女',
  country varchar(64) DEFAULT NULL COMMENT '国家',
  state varchar(64) DEFAULT NULL COMMENT '洲',
  portrait varchar(128) DEFAULT NULL COMMENT '头像',
  city varchar(64) DEFAULT NULL COMMENT '城市',
  password varchar(256) NOT NULL COMMENT '登录密码',
  pay_password varchar(256) DEFAULT NULL COMMENT '支付密码',
  telephone varchar(64) DEFAULT NULL COMMENT '电话',
  Identity int NOT NULL DEFAULT '1' COMMENT '身份 1.用户 2.管理员',
  airport_id bigint DEFAULT NULL COMMENT '机场Id',
  create_date datetime DEFAULT NULL COMMENT '创建时间',
  create_by bigint DEFAULT NULL COMMENT '创建人',
  update_datae datetime DEFAULT NULL COMMENT '修改时间',
  update_by bigint DEFAULT NULL COMMENT '修改人',
  mileage_num int NOT NULL DEFAULT '0' COMMENT '里程数',
  PRIMARY KEY (customer_id),
  UNIQUE KEY email (email)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='客户表';

/*Table structure for table flight_ */

DROP TABLE IF EXISTS flight_;

CREATE TABLE flight_ (
  flight_id bigint NOT NULL AUTO_INCREMENT,
  airline_id bigint NOT NULL COMMENT '航空公司id',
  flight_number varchar(128) DEFAULT NULL COMMENT '航班号',
  flight_date datetime DEFAULT NULL COMMENT '航班日期',
  start_address varchar(128) DEFAULT NULL COMMENT '起飞地址',
  destination_ varchar(128) DEFAULT NULL COMMENT '目的地',
  start_date datetime DEFAULT NULL COMMENT '起飞时间',
  arrive_date datetime DEFAULT NULL COMMENT '到达时间',
  first_class_num int NOT NULL DEFAULT '0' COMMENT '头等舱数量',
  economy_class_num int NOT NULL DEFAULT '0' COMMENT '经济舱数量',
  first_class_price decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '头等舱价格',
  economy_class_price decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '经济舱价格',
  mileage_sum int DEFAULT NULL COMMENT '里程',
  departure_airport bigint NOT NULL COMMENT '起飞机场',
  destination_airport bigint NOT NULL COMMENT '到达机场',
  PRIMARY KEY (flight_id)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COMMENT='航班';

/*Table structure for table mileage */

DROP TABLE IF EXISTS mileage;

CREATE TABLE mileage (
  mileage_id bigint NOT NULL AUTO_INCREMENT,
  flight_id bigint NOT NULL COMMENT '航班id',
  first_mileage int NOT NULL DEFAULT '0' COMMENT '经济舱里程数',
  economy_mileage int NOT NULL DEFAULT '0' COMMENT '头等舱',
  create_date datetime DEFAULT NULL COMMENT '创建时间',
  update_date datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (mileage_id),
  UNIQUE KEY flight_id (flight_id)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COMMENT='里程规则';

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
